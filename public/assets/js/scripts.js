$(function () {
    const elements = document.querySelectorAll(".auction_countdown");
    Array.from(elements).forEach((element, index) => {
        startCounter(element);
    });

    function startCounter(counterElement) {
        let endDate = $(counterElement).attr("data-end");

        let myCountDown = new ysCountDown(endDate, function (
            remaining,
            finished
        ) {
            let message = "";
            if (finished) {
                message = "Expired";
            } else {
                var re_days = remaining.totalDays;
                var re_hours = remaining.hours;
                message += re_days + "d  : ";
                message += re_hours + "h  : ";
                message += remaining.minutes + "m  : ";
                message += remaining.seconds + "s";
            }
            $(counterElement).html(message);
        });
    }
});
