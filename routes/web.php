<?php

use App\Http\Controllers\WebUser\AuctionController;
use App\Http\Controllers\WebUser\HomeController;
use App\Http\Controllers\WebUser\AuthController;
use App\Http\Controllers\webuser\BuyerController;
use App\Http\Controllers\WebUser\PaymentController;
use App\Http\Controllers\WebUser\SellerController;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Public routes
Route::get('/seller-signup', [AuthController::class, 'sellerRegistration'])->name('seller-signup');
Route::get('/buyer-signup', [AuthController::class, 'buyerRegistration'])->name('buyer-signup');
Route::get('/proceed-reg-payment', [AuthController::class, 'regPaymentForm'])->name('reg-payment_form');
Route::get('/payment-response', [PaymentController::class, 'paymentResponse']);
Route::get('/about-us', [HomeController::class, 'about'])->name('about');
Route::get('/contact', [HomeController::class, 'contact'])->name('contact');
Route::post('/contact', [HomeController::class, 'sendContact'])->name('contact.send');

Route::middleware(['reg_fee_check'])->group(function () {
    Route::get('/', [HomeController::class, 'index'])->name('home');
    Route::get('/auction/{category_id?}', [AuctionController::class, 'auctionList'])->name('auction');
    Route::get('/auction_view/{product_id}', [AuctionController::class, 'viewAuction'])->name('auction_view');

    Route::middleware(['auth'])->group(function () {
        Route::post('/physical-inspection-submit', [AuctionController::class, 'phyInspectonSubmit'])->name('physical-inspection-submit');
        Route::post('/bid-submit', [AuctionController::class, 'phyInspectonSubmit'])->name('bid-submit');

        //authenticated seller routes
        Route::middleware(['auth', 'seller'])->group(function () {
            Route::get('/seller-account', [SellerController::class, 'sellerAccount'])->name('seller-account');
            Route::get('/edit-profile', [AuthController::class, 'editProfile'])->name('edit-profile');
            Route::get('/product-create', [SellerController::class, 'createProduct'])->name('product-create');
            Route::get('/seller-inspection-view/{inspection_id}', [SellerController::class, 'phyInspection'])->name('seller-inspection-view');
            Route::get('/seller-inspection', [SellerController::class, 'reqInspection'])->name('seller-inspection');
            Route::get('/seller-products', [SellerController::class, 'sellerProduct'])->name('seller-products');
            Route::get('/seller-product-bids/{product_id}', [SellerController::class, 'sellerProductBids'])->name('seller-product-bids');
            Route::get('/seller-transactions', [SellerController::class, 'sellerTransactions'])->name('seller-transactions');
        });

        //authenticated buyer routes
        Route::middleware(['auth', 'buyer'])->group(function () {
            Route::get('/buyer-account', [BuyerController::class, 'buyerAccount'])->name('buyer-account');
            Route::get('/profile', [HomeController::class, 'profile'])->name('profile');
            Route::get('/edit-profile-buyer', [AuthController::class, 'editProfileBuyer'])->name('edit-profile-Buyer');
            Route::get('/buyer-inspection', [BuyerController::class, 'buyerInspection'])->name('buyer-inspection');
            Route::get('/buyer-inspection-view/{inspection_id}', [BuyerController::class, 'buyerInspectionView'])->name('buyer-inspection-view');
            Route::get('/buyer-products', [BuyerController::class, 'buyerProducts'])->name('buyer-products');
            Route::get('/buyer-product-bids/{product_id}', [BuyerController::class, 'buyerProductBids'])->name('buyer-product-bids');
            Route::get('/buyer-transaction', [BuyerController::class, 'buyerTransaction'])->name('buyer-transaction');
            Route::get('/buyer-win-products', [BuyerController::class, 'buyerWinProducts'])->name('buyer-win-products');
        });
    });
});

// Authentication Routes...
Route::get('login', [AuthController::class, 'userLogin'])->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
Route::post('register', 'Auth\RegisterController@register')->name('register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');
Route::get('email/verify', 'Auth\VerificationController@show')->name('verification.notice');
Route::get('email/verify/{id}', 'Auth\VerificationController@verify')->name('verification.verify');
Route::get('email/resend', 'Auth\VerificationController@resend')->name('verification.resend');
