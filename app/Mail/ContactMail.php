<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactMail extends Mailable
{
    use Queueable, SerializesModels;

    public $data = null;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $mail_subject = 'Contact Mail From '. $this->data['name'] ;

        $mail = $this->subject($mail_subject)
            ->from([$this->data['email']])
            ->view('email.contact')
            ->with([
                'data' => $this->data
            ]);

        return $mail;
    }
}
