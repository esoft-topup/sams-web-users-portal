<?php

namespace App\Http\Controllers\WebUser;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\City;
use App\Models\CompanyWallet;
use App\Models\MembershipFee;
use App\Models\SystemMeta;
use App\Models\User;
use App\Models\WebUser;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function sellerRegistration()
    {
        $cities = City::pluck('city_name', 'id');
        return view('seller-signup', compact('cities'));
    }

    public function buyerRegistration()
    {
        $cities = City::pluck('city_name', 'id');
        return view('buyer-signup', compact('cities'));
    }

    public function userLogin()
    {
        return view('user-login');
    }
    public function editProfile()
    {
        $cities = City::pluck('city_name', 'id');
        return view('edit-profile', compact('cities'));

    }
    public function editProfileBuyer()
    {
        $cities = City::pluck('city_name', 'id');
        return view('buyer.edit-profile-buyer', compact('cities'));

    }

    public function regPaymentForm()
    {
        if(!isset(Auth::user()->email_verified_at)){
            return redirect('login')->with('message', 'You are not a registered user. You will receive a confirmation email. Kindly verify to continue.');
        }

        $fees = MembershipFee::where('web_users_id', Auth::user()->id)->where('transaction_type', 'REG_FEE')->first();
        if(isset($fees)){
            return redirect()->to('/');
        }

        $reg_fee_meta    = SystemMeta::where('meta_name', 'REG_FEE')->first();
        $annual_fee_meta = SystemMeta::where('meta_name', 'ANNUAL_FEE')->first();
        $total_amount = (float) $reg_fee_meta->meta_value + (float) $annual_fee_meta->meta_value;

        DB::table('temp_transactions')->insert([
            'type'      => 'REG_PAY',
            'user_id'   => Auth::user()->id,
            'amount'    => number_format($total_amount, 2, '.', ''),
            'reference' => '-',
            'status'    => 'PENDING'
        ]);
        $transaction = DB::table('temp_transactions')->orderBy('id', 'DESC')->first();
        return view('reg-payment', compact('reg_fee_meta', 'annual_fee_meta', 'transaction'));
    }
}
