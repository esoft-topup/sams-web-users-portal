<?php

namespace App\Http\Controllers\WebUser;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\ProductsTrait;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SellerController extends Controller
{
    use ProductsTrait;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function sellerAccount()
    {
        return view('seller-account',['active_menu'=>2]);
    }

    public function sellerProduct()
    {
        $products = DB::table('products')
        ->whereNull('deleted_at')
        ->orderBy('created_at', 'DESC')
        ->get();
        $products = $this->formatedProducts($products);
        return view('seller-products', compact('products'), ['button' => 'view-bids', 'active_menu'=> 3]);
    }

    public function sellerProductBids($product_id)
    {
        $biddings = DB::table('biddings')->orderBy('products_id')->orderBy('bidded_value', 'DESC')->get();
        return view('seller-product-bids', compact('biddings'),['active_menu'=> 3]);
    }

    public function createProduct()
    {
        return view('product-create', ['active_menu'=> 3]);
    }
    
    public function reqInspection()
    {
        return view('seller-inspection-view', ['active_menu'=> 5]);
    }
    

    public function phyInspection()
    {
        return view('inspection',['active_menu'=>5]);
    }

    public function sellerTransactions()
    {
        return view('seller-transactions',['active_menu'=>1]);
    }
}
