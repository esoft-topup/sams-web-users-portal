<?php

namespace App\Http\Controllers\WebUser;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\ProductsTrait;
use App\Models\Bidding;
use App\Models\Category;
use App\Models\City;
use App\Models\CityDistance;
use App\Models\PhyInspection;
use App\Models\Product;
use App\Models\ProductView;
use App\Models\SellerHasProdutc;
use App\Models\SystemMeta;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class AuctionController extends Controller
{
    use ProductsTrait;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function auctionList(Request $request, $category_id = null)
    {
        $min_price = $request->query('min_price');
        $max_price = $request->query('max_price');
        $search    = $request->query('search');

        $min_price = isset($min_price) ? $min_price : 0;
        $max_price = isset($max_price) ? $max_price : 999999999999;
        $search    = isset($search)    ? $search : '';

        $category = isset($category_id) ? Category::find($category_id) : null;

        $now = Carbon::now("Asia/Colombo")->toDateTimeString();

        if (isset($category_id) && !empty($category_id)) {

            //get most viewed 3 products by category
            $featured_products = DB::table('products')
                ->select('products.id', 'product_name', 'closing_date_time', 'minimum_price', 'status', 'deleted_at', DB::raw('SUM(count) AS views'))
                ->leftJoin('product_views', 'products.id', '=', 'product_views.products_id')
                ->where('categories_id', $category_id)
                ->whereNull('deleted_at')
                ->where('status', 1)
                ->where('start_date_time', '<=', $now)
                ->where('closing_date_time', '>=', $now)
                ->groupBy('products.id')
                ->orderBy('views', 'DESC')
                ->limit(3)
                ->get();

            //get paginated products by category and decending by created date
            $products = DB::table('products')
                ->select('products.id', 'product_name', 'closing_date_time', 'minimum_price', 'status', 'deleted_at', DB::raw('CASE WHEN closing_date_time > NOW() THEN 1 ELSE 0 END AS expired'))
                ->where('categories_id', $category_id)
                ->whereNull('deleted_at')
                ->where('status', 1)
                ->where('start_date_time', '<=', $now)
                ->whereBetween('minimum_price', [$min_price, $max_price])
                ->where('product_name', 'like', '%' . $search . '%')
                ->orderBy('created_at', 'DESC')
                ->orderBy('expired', 'DESC')
                ->paginate(6)
                ->through(fn ($product) => $this->productReFormat($product));
        } else {
            //get most viewed 3 products
            $featured_products = DB::table('products')
                ->select('products.id', 'product_name', 'closing_date_time', 'minimum_price', 'status', 'deleted_at', DB::raw('SUM(count) AS views'))
                ->leftJoin('product_views', 'products.id', '=', 'product_views.products_id')
                ->whereNull('deleted_at')
                ->where('status', 1)
                ->where('start_date_time', '<=', $now)
                ->where('closing_date_time', '>=', $now)
                ->groupBy('products.id')
                ->orderBy('views', 'DESC')
                ->limit(3)
                ->get();

            //get all paginated products and decending by created date
            $products = DB::table('products')
                ->select('products.id', 'product_name', 'closing_date_time', 'minimum_price', 'status', 'deleted_at', DB::raw('CASE WHEN closing_date_time > NOW() THEN 1 ELSE 0 END AS expired'))
                ->whereNull('deleted_at')
                ->where('status', 1)
                ->where('start_date_time', '<=', $now)
                ->whereBetween('minimum_price', [$min_price, $max_price])
                ->where('product_name', 'like', '%' . $search . '%')
                ->orderBy('created_at', 'DESC')
                ->orderBy('expired', 'DESC')
                ->paginate(6)
                ->through(fn ($product) => $this->productReFormat($product));
        }

        $featured_products = $this->formatedProducts($featured_products);

        return view('auction', compact('featured_products', 'products', 'category', 'min_price', 'max_price', 'search'));
    }

    public function viewAuction($product_id)
    {
        $product = Product::find($product_id);
        if (!isset($product)) abort(404, 'Not found');

        $buyer = Auth::user();
        $is_buyer = isset($buyer) && $buyer->roles_id == 1;

        $product['end_date_time'] = Carbon::createFromFormat('Y-m-d H:i:s', $product->closing_date_time);

        $biddings = Bidding::where('products_id', $product_id)->orderBy('bidded_value', 'DESC')->get();
        if (isset($biddings) && count($biddings) > 0) {
            $product['min_bid'] = $biddings[0]->bidded_value;
        } else {
            $product['min_bid'] = $product->minimum_price;
        }

        $product_views = ProductView::where('products_id', $product_id)->first();
        if (!isset($product_views)) {
            $product_views = ProductView::create([
                'count' => 1,
                'products_id' => $product_id
            ]);
        } else {
            $product_views->count += 1;
            $product_views->save();
        }

        $delivery_meta = SystemMeta::where('meta_name', 'DELIVERY_FEE_PER_KM')->first();
        $min_increment_meta = SystemMeta::where('meta_name', 'BID_MINIMUM_INCREMENT')->first();
        $other_info = ['min_increment' => $min_increment_meta->meta_value];

        $seller_product = SellerHasProdutc::where('products_id', $product_id)->first();
        $seller = $seller_product->seller;

        $other_info['seller_location'] = (City::find($seller->cities_id))->city_name;

        if (isset($buyer)) {
            $buyer_city_name  = isset($buyer) ? City::find($buyer->cities_id)->city_name : '';
            $other_info['buyer_location'] = $buyer_city_name;

            $delivery_km = CityDistance::where(['city_a_id' => $seller->cities_id, 'city_b_id' => $buyer->cities_id])->orWhere(['city_a_id' => $buyer->cities_id, 'city_b_id' => $seller->cities_id])->first();
            $delivery_km = isset($delivery_km) ? (int) $delivery_km->distance_km : 1;
            $delivery_fee = (float) $delivery_meta->meta_value * $delivery_km;
            $other_info['delivery_fee'] = number_format($delivery_fee, 2, '.', ',');
        }

        //check auction expired
        $now = strtotime(Carbon::now("Asia/Colombo")->toDateTimeString());
        $expire = strtotime($product->closing_date_time);
        $is_expired = $now > $expire;

        return view('auction_view', compact('product', 'product_views', 'biddings', 'seller', 'other_info', 'is_expired', 'is_buyer'));
    }

    public function phyInspectonSubmit(Request $request)
    {
        Validator::make($request->all(), [
            'inspection_datetime'   => ['required', 'date_format:Y-m-d\TH:i'],
            'note'  => ['required', 'string', 'min:20'],
            'products_id' => ['required'],
        ], [], [
            'inspection_datetime'   => 'Inspection Date & Time',
            'note'  => 'Note for Seller',
        ])->validate();

        PhyInspection::create([
            'inspection_datetime' => $request->inspection_datetime,
            'note' => $request->note,
            'agreed' => false,
            'products_id' => $request->products_id,
            'web_users_id' => Auth::user()->id
        ]);

        return redirect()->to('/auction_view/'.$request->products_id)->with('success', 'You request has been submitted. Please check your profile to see the request status.');
    }
}
