<?php

namespace App\Http\Controllers\WebUser;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\SystemMeta;
use App\Models\CompanyWallet;
use App\Models\MembershipFee;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;

class PaymentController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function paymentResponse(Request $request)
    {
        $transaction = DB::table('temp_transactions')->where('id', $request->order_id)->first();

        if($transaction->type == 'REG_PAY'){
            $reg_fee_meta    = SystemMeta::where('meta_name', 'REG_FEE')->first();
            $annual_fee_meta = SystemMeta::where('meta_name', 'ANNUAL_FEE')->first();
            $total_amount = (float) $reg_fee_meta->meta_value + (float) $annual_fee_meta->meta_value;
    
            MembershipFee::create([
                'transaction_type' => 'REG_FEE',
                'amount'           => number_format((float) $reg_fee_meta->meta_value , 2, '.', ''),
                'companies_id'     => '1',
                'web_users_id'     => $transaction->user_id,
                ]);
    
                MembershipFee::create([
                    'transaction_type' => 'ANNUAL_FEE',
                    'amount'           => number_format((float) $annual_fee_meta->meta_value, 2, '.', ''),
                'companies_id'     => '1',
                'web_users_id'     => $transaction->user_id,
            ]);
    
            $wallet = CompanyWallet::where('companies_id', '1')->first();
            $wallet->amount = number_format(((float) $wallet->amount + $total_amount), 2, '.', '');
            $wallet->save();
    
            DB::table('temp_transactions')
              ->where('id', $request->order_id)
              ->update(['status' => 'SUCCESS']);
            Session::flash('reg_fee_success', 'success');
            return Redirect::to('/');
        }
        
    }
}
