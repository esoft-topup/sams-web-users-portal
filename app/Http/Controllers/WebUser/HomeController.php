<?php

namespace App\Http\Controllers\WebUser;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\ProductsTrait;
use App\Models\Bidding;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactMail;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    use ProductsTrait;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $categoriIds = DB::table('products')
        ->join('product_views', 'products.id', '=', 'product_views.products_id')->select(DB::raw("SUM(count) as views"), 'categories_id')->groupBy('categories_id')->orderBy('views', 'DESC')->limit('3')->pluck('categories_id');
        $product_categories = [];
        foreach ($categoriIds as $categoriId){
            $product_categories[] = Category::where('id', $categoriId)->first();
        }
        $now = Carbon::now("Asia/Colombo")->toDateTimeString();
        foreach ($product_categories as $category) {
            $products = Product::leftJoin('product_views', function ($join) {
                $join->on('products.id', '=', 'product_views.products_id');
            })->select('products.id as id', 'product_name', 'closing_date_time', 'minimum_price', 'status', 'deleted_at')
            ->where('categories_id', $category->id)
            ->where('status', 1)
            ->where('start_date_time', '<=', $now)
            ->where('closing_date_time', '>=', $now)
            ->orderBy('count', 'DESC')
            ->limit(3)
            ->get();
            $category['products'] = $this->formatedProducts($products);
        }

        $won_bids = Bidding::where('is_win', 1)->orderBy('bidded_time', 'desc')->limit(6)->get();
        foreach($won_bids as $won_bid){
            $product = Product::find($won_bid->products_id);
            $won_bid['product'] = $this->wonProductReFormat($won_bid, $product);
        }
        return view('home', compact('product_categories', 'won_bids'));
    }

    public function profile()
    {
        return view('profile');
    }

    public function about()
    {
        return view('about');
    }

    public function contact()
    {
        return view('contact');
    }

    public function sendContact(Request $request){
        
        $data = $request->validate([
            'name' => 'required|string',
            'email' => 'required|email',
            'message' => 'required|string',
        ]);

        $mail_sent = Mail::to(['admin@samsauction.com'])->send(new ContactMail($data));

      
        return redirect()->back();
    }

}
