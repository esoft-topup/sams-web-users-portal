<?php

namespace App\Http\Controllers\WebUser;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Category;
use App\Http\Controllers\Traits\ProductsTrait;

class BuyerController extends Controller
{
    use ProductsTrait;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    public function buyerAccount()
    {
        return view('buyer.buyer-account',['active_menu'=>2]);;
    }

    public function buyerProducts()
    {
        $products = DB::table('products')
        ->whereNull('deleted_at')
        ->orderBy('created_at', 'DESC')
        ->limit(5)
        ->get();
        $products = $this->formatedProducts($products);
        return view('buyer.buyer-products', compact('products'), ['button' => 'view-bids', 'active_menu'=> 3]);
    }

    public function buyerProductBids($product_id)
    {
        $biddings = DB::table('biddings')->orderBy('products_id')->orderBy('bidded_value', 'DESC')->get();
        return view('buyer.buyer-product-bids', compact('biddings'));
    }

    public function editProfileBuyer()
    {
        return view('buyer.edit-profile-buyer',['active_menu'=>2]);
    }

    public function buyerInspection()
    {
        return view('buyer.buyer-inspection',['active_menu'=>5]);
    }
    public function buyerInspectionView()
    {
        return view('buyer.buyer-inspection-view',['active_menu'=>5]);
    }

    public function buyerTransaction()
    {
        return view('buyer.buyer-transaction',['active_menu'=>1]);
    }
    public function buyerWinProducts()
    {
        return view('buyer.buyer-win-products',['active_menu'=>4]);
    }
    public function buyerPayProducts()
    {
        return view('buyer.buyer-pay-products',['active_menu'=>5]);
    }

}
