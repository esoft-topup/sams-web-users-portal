<?php

namespace App\Http\Controllers\Traits;

use App\Models\Bidding;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

trait ProductsTrait
{
    public function productReFormat($product)
    {
        if(isset($product->images) && isset($product->images[0])){
            $image = $product->images[0]->getUrl();
        } else {
            $media = Media::where('model_type', 'App\Models\Product')->where('model_id', $product->id)->first();
            $image = isset($media) ? 'https://bidding-web.s3.ap-south-1.amazonaws.com/' . $media->id . '/' . $media->file_name : false;
        }
        $bidding_count = Bidding::where('products_id', $product->id)->count();
        $close_date = Carbon::createFromFormat('Y-m-d H:i:s', $product->closing_date_time);
        $p_array = [];
        if(!isset($product->deleted_at) && $product->status == 1){
            $p_array = [
                'id'            => $product->id,
                'product_name'  => $product->product_name,
                'end_date_time' => $close_date,
                'image'         => $image,
                'minimum_price' => 'LKR ' . number_format($product->minimum_price, 2, '.', ','),
                'bidding_count' => $bidding_count
            ];
        }
        return (object) $p_array;
    }

    public function formatedProducts($products)
    {
        $formated_array = [];
        foreach ($products as $product) {
            $formated = $this->productReFormat($product);
            if(isset($formated) && isset($formated->id)){
                $formated_array[] = $formated;
            }

        }
        return $formated_array;
    }

    public function wonProductReFormat($bid, $product)
    {
        if(isset($product->images) && isset($product->images[0])){
            $image = $product->images[0]->getUrl();
        } else {
            $media = Media::where('model_type', 'App\Models\Product')->where('model_id', $product->id)->first();
            $image = isset($media) ? 'https://bidding-web.s3.ap-south-1.amazonaws.com/' . $media->id . '/' . $media->file_name : false;
        }
        $bidding_count = Bidding::where('products_id', $product->id)->count();
        if(!isset($product->deleted_at) && $product->status == 1){
            $p_array = [
                'id'            => $product->id,
                'product_name'  => $product->product_name,
                'image'         => $image,
                'won_price' => 'LKR ' . number_format($bid->bidded_value, 2, '.', ','),
                'bidding_count' => $bidding_count,
                'winner'       => $bid->bidder->first_name
            ];
        }
        return (object) $p_array;
    }

}
