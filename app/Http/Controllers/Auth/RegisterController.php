<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\RegPaymentEmail;
use App\Models\Wallet;
use App\Providers\RouteServiceProvider;
use App\Models\WebUser;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name'  => ['required', 'string', 'max:255'],
            'nic'        => ['required', 'string', 'max:12', 'unique:web_users'],
            'address'    => ['required', 'string', 'min:20'],
            'contact'    => ['required', 'string', 'max:10', 'unique:web_users'],
            'email'      => ['required', 'string', 'email', 'max:255', 'unique:web_users'],
            'password'   => ['required', 'string', 'min:8', 'confirmed'],
            'roles_id'   => ['required', 'string', 'min:1'],
            'cities_id'  => ['required', 'string', 'min:1'],
        ], [], [
            'first_name' => 'First Name',
            'last_name'  => 'Last Name',
            'roles_id'   => 'Role',
            'cities_id'  => 'City',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\WebUser
     */
    protected function create(array $data)
    {
        return WebUser::create([
            'first_name' => $data['first_name'],
            'last_name'  => $data['last_name'],
            'nic'        => $data['nic'],
            'address'    => $data['address'],
            'contact'    => $data['contact'],
            'email'      => $data['email'],
            'password'   => Hash::make($data['password']),
            'roles_id'   => $data['roles_id'],
            'cities_id'  => $data['cities_id'],
            'status'     => 1,
        ]);
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        $user = $this->create($request->all());

        Wallet::create(['amount' => '0.00', 'web_users_id' => $user->id]);

        event(new Registered($user));

        return redirect(url()->previous())->with('message', 'You will receive a confirmation email. Kindly verify to continue.');
    }
}
