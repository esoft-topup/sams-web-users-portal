<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\RegPaymentEmail;
use App\Models\BlackList;
use App\Models\MembershipFee;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    protected function authenticated(Request $request, $user)
    {
        if (!$user->hasVerifiedEmail()) {
            event(new Registered($user));
            Auth::logout();
            return redirect()->to('login')->with('message', 'You will receive a confirmation email. Kindly verify to continue.');
        }

        $blackList = BlackList::where('web_users_id', $user->id)->first();
        if (isset($blackList)) {
            Auth::logout();
            return redirect()->to('login')->with('message', 'Your account has been blocked by admin. Please contact to unblock.');
        }

        $fees = MembershipFee::where('web_users_id', $user->id)->where('transaction_type', 'REG_FEE')->first();
        if (!isset($fees)) {
            Mail::to($user->email)->send(new RegPaymentEmail($user->id));
            return redirect()->to('/proceed-reg-payment/')->with('error', 'Kindly pay required payments for continue.');
        }

        return redirect()->to('/');
    }
}
