<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BuyerChecker
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $role_id = Auth::check() ? Auth::user()->roles_id : 0;
        if($role_id == 1){
            return $next($request);
        } else {
            abort(403, 'Access denied');
        }
    }
}
