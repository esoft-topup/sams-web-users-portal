<?php

namespace App\Http\Middleware;

use App\Models\MembershipFee;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RegPaymentChecker
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(Auth::check()){
            $fees = MembershipFee::where('web_users_id', Auth::user()->id)->where('transaction_type', 'REG_FEE')->first();
            if(isset($fees)){
                return $next($request);
            }else{
                return redirect()->to('/proceed-reg-payment/')->with('error', 'Kindly pay required payments for continue.');
            }
        } else {
            return $next($request);
        }
    }
}
