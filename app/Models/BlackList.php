<?php

namespace App\Models;

use \DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BlackList extends Model
{
    use HasFactory;

    public $table = 'black_lists';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'web_users_id',
        'blocked_by_id',
        'reason',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function web_users()
    {
        return $this->belongsTo(WebUser::class, 'web_users_id');
    }

    public function blocked_by()
    {
        return $this->belongsTo(User::class, 'blocked_by_id');
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
