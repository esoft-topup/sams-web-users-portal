<?php

namespace App\Models;

use \DateTimeInterface;
use Hash;
use Carbon\Carbon;
use Illuminate\Auth\Notifications\ResetPassword;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class WebUser extends Authenticatable implements MustVerifyEmail
{
    use SoftDeletes;
    use HasFactory;
    use Notifiable;

    public const STATUS_SELECT = [
        '1' => 'ACTIVE',
        '0' => 'INACTIVE',
    ];

    public $table = 'web_users';

    protected $hidden = [
        'remember_token',
        'password',
    ];

    protected $dates = [
        'email_verified_at',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'roles_id',
        'cities_id',
        'first_name',
        'last_name',
        'nic',
        'address',
        'contact',
        'email',
        'email_verified_at',
        'password',
        'remember_token',
        'status',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function webUsersPhyInspections()
    {
        return $this->hasMany(PhyInspection::class, 'web_users_id', 'id');
    }

    public function webUsersBiddings()
    {
        return $this->hasMany(Bidding::class, 'web_users_id', 'id');
    }

    public function sellerTransactions()
    {
        return $this->hasMany(Transaction::class, 'seller_id', 'id');
    }

    public function senderMessages()
    {
        return $this->hasMany(Message::class, 'sender_id', 'id');
    }

    public function receiverMessages()
    {
        return $this->hasMany(Message::class, 'receiver_id', 'id');
    }

    public function role()
    {
        return $this->belongsTo(WebUserRole::class, 'roles_id');
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'cities_id');
    }

    public function roles()
    {
        return $this->belongsTo(WebUserRole::class, 'roles_id');
    }

    public function getEmailVerifiedAtAttribute($value)
    {
        return $value ? Carbon::parse($value)->format('Y-m-d H:i:s') : null;
    }

    public function setEmailVerifiedAtAttribute($value)
    {
        $this->attributes['email_verified_at'] = $value ? Carbon::parse($value)->format('Y-m-d H:i:s') : null;
    }

    public function setPasswordAttribute($input)
    {
        if ($input) {
            $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
        }
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
