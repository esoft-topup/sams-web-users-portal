<?php

namespace App\Models;

use \DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    public $table = 'transactions';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'seller_id',
        'biddings_id',
        'seller_amount',
        'company_commission',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function seller()
    {
        return $this->belongsTo(WebUser::class, 'seller_id');
    }

    public function biddings()
    {
        return $this->belongsTo(Bidding::class, 'biddings_id');
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
