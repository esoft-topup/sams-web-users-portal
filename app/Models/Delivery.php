<?php

namespace App\Models;

use \DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Delivery extends Model
{
    use SoftDeletes;
    use HasFactory;

    public const DELIVERY_STATUS_SELECT = [
        '0' => 'PROCESSING',
        '1' => 'DISPATCHED',
        '2' => 'DELIVERED',
    ];

    public $table = 'deliveries';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'product_id',
        'seller_id',
        'seller_address',
        'buyer_id',
        'buyer_address',
        'delivery_status',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function seller()
    {
        return $this->belongsTo(WebUser::class, 'seller_id');
    }

    public function buyer()
    {
        return $this->belongsTo(WebUser::class, 'buyer_id');
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
