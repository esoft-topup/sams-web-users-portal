<?php

namespace App\Models;

use \DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MembershipFee extends Model
{
    use SoftDeletes;
    use HasFactory;

    public const TRANSACTION_TYPE_SELECT = [
        'ANNUAL_FEE' => 'Annual Fee',
        'REG_FEE'    => 'Registration Fee',
    ];

    public $table = 'membership_fees';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'companies_id',
        'web_users_id',
        'transaction_type',
        'amount',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function companies()
    {
        return $this->belongsTo(Company::class, 'companies_id');
    }

    public function web_users()
    {
        return $this->belongsTo(WebUser::class, 'web_users_id');
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
