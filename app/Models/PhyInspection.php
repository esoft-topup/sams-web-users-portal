<?php

namespace App\Models;

use \DateTimeInterface;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PhyInspection extends Model
{
    use HasFactory;

    public $table = 'phy_inspections';

    protected $dates = [
        'inspection_datetime',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'web_users_id',
        'products_id',
        'inspection_datetime',
        'note',
        'agreed',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function web_users()
    {
        return $this->belongsTo(WebUser::class, 'web_users_id');
    }

    public function products()
    {
        return $this->belongsTo(Product::class, 'products_id');
    }

    public function getInspectionDatetimeAttribute($value)
    {
        return $value ? Carbon::createFromFormat('Y-m-d H:i:s', $value)->format(config('panel.date_format') . ' ' . config('panel.time_format')) : null;
    }

    public function setInspectionDatetimeAttribute($value)
    {
        $this->attributes['inspection_datetime'] = $value ? Carbon::createFromFormat(config('panel.date_format') . ' ' . config('panel.time_format'), $value)->format('Y-m-d H:i:s') : null;
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
