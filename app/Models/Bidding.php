<?php

namespace App\Models;

use \DateTimeInterface;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bidding extends Model
{
    use SoftDeletes;
    use HasFactory;

    public $table = 'biddings';

    protected $dates = [
        'bidded_time',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'web_users_id',
        'products_id',
        'bidded_value',
        'bidded_time',
        'is_win',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function bidder()
    {
        return $this->belongsTo(WebUser::class, 'web_users_id');
    }

    public function products()
    {
        return $this->belongsTo(Product::class, 'products_id');
    }

    public function getBiddedTimeAttribute($value)
    {
        return $value ? Carbon::createFromFormat('Y-m-d H:i:s', $value)->format(config('panel.date_format') . ' ' . config('panel.time_format')) : null;
    }

    public function setBiddedTimeAttribute($value)
    {
        $this->attributes['bidded_time'] = $value ? Carbon::createFromFormat(config('panel.date_format') . ' ' . config('panel.time_format'), $value)->format('Y-m-d H:i:s') : null;
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
