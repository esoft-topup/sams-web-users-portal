<?php

namespace App\Models;

use \DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SellerHasProdutc extends Model
{
    use HasFactory;

    public $table = 'seller_has_produtcs';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'web_users_id',
        'products_id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function seller()
    {
        return $this->belongsTo(WebUser::class, 'web_users_id');
    }

    public function products()
    {
        return $this->belongsTo(Product::class, 'products_id');
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
