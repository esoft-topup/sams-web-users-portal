<?php

namespace App\Models;

use \DateTimeInterface;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CityDistance extends Model
{
    use HasFactory;

    public $table = 'city_distances';

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'city_a_id',
        'city_b_id',
        'distance_km',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function city_a()
    {
        return $this->belongsTo(City::class, 'city_a_id');
    }

    public function city_b()
    {
        return $this->belongsTo(City::class, 'city_b_id');
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
