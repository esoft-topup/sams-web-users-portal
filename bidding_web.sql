/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.7.33-log : Database - bidding_web
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `biddings` */

DROP TABLE IF EXISTS `biddings`;

CREATE TABLE `biddings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `bidded_value` decimal(15,2) NOT NULL,
  `bidded_time` datetime NOT NULL,
  `is_win` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `web_users_id` bigint(20) unsigned NOT NULL,
  `products_id` bigint(20) unsigned NOT NULL,
  `is_deposit` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `web_users_fk_4554342` (`web_users_id`),
  KEY `products_fk_4554343` (`products_id`),
  CONSTRAINT `products_fk_4554343` FOREIGN KEY (`products_id`) REFERENCES `products` (`id`),
  CONSTRAINT `web_users_fk_4554342` FOREIGN KEY (`web_users_id`) REFERENCES `web_users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `biddings` */

insert  into `biddings`(`id`,`bidded_value`,`bidded_time`,`is_win`,`created_at`,`updated_at`,`deleted_at`,`web_users_id`,`products_id`,`is_deposit`) values (1,5000.00,'2021-08-18 03:04:51',1,'2021-08-18 03:04:56','2021-08-18 03:04:58',NULL,14,7,1),(2,5000.00,'2021-08-18 03:04:51',1,'2021-08-18 03:04:56','2021-08-18 03:04:58',NULL,14,8,1),(3,5000.00,'2021-08-18 03:04:51',1,'2021-08-18 03:04:56','2021-08-18 03:04:58',NULL,14,9,1),(4,5000.00,'2021-08-18 03:04:51',1,'2021-08-18 03:04:56','2021-08-18 03:04:58',NULL,14,10,1),(5,5000.00,'2021-08-18 03:04:51',1,'2021-08-18 03:04:56','2021-08-18 03:04:58',NULL,14,11,1),(6,5000.00,'2021-08-18 03:04:51',1,'2021-08-18 03:04:56','2021-08-18 03:04:58',NULL,14,12,1);

/*Table structure for table `black_lists` */

DROP TABLE IF EXISTS `black_lists`;

CREATE TABLE `black_lists` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `reason` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `web_users_id` bigint(20) unsigned NOT NULL,
  `blocked_by_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `web_users_fk_4554075` (`web_users_id`),
  KEY `blocked_by_fk_4554076` (`blocked_by_id`),
  CONSTRAINT `blocked_by_fk_4554076` FOREIGN KEY (`blocked_by_id`) REFERENCES `users` (`id`),
  CONSTRAINT `web_users_fk_4554075` FOREIGN KEY (`web_users_id`) REFERENCES `web_users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `black_lists` */

insert  into `black_lists`(`id`,`reason`,`created_at`,`updated_at`,`web_users_id`,`blocked_by_id`) values (1,'TEstest','2021-08-17 17:01:44','2021-08-17 17:01:44',14,1);

/*Table structure for table `categories` */

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_category_name_unique` (`category_name`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `categories` */

insert  into `categories`(`id`,`category_name`,`created_at`,`updated_at`) values (1,'Electronics',NULL,'2021-08-14 06:56:16'),(2,'Books',NULL,NULL),(3,'Furnitures',NULL,NULL),(4,'Fashion',NULL,'2021-08-14 06:30:23'),(5,'Jewelry',NULL,'2021-08-14 06:29:55'),(6,'Sport Items','2021-08-14 06:56:35','2021-08-14 06:56:35'),(7,'Baby Items','2021-08-14 06:56:56','2021-08-14 06:56:56');

/*Table structure for table `cities` */

DROP TABLE IF EXISTS `cities`;

CREATE TABLE `cities` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `city_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cities_city_name_unique` (`city_name`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `cities` */

insert  into `cities`(`id`,`city_name`,`created_at`,`updated_at`) values (1,'Kadawatha',NULL,NULL),(2,'Kiribathgoda',NULL,NULL),(3,'Gampaha',NULL,NULL),(4,'Ganemulla',NULL,NULL),(5,'Kirillawala',NULL,NULL),(6,'Ragama',NULL,NULL),(7,'Batuwaththa',NULL,NULL),(8,'Eldeniya',NULL,NULL),(9,'Miriswaththa',NULL,NULL),(10,'Waththala',NULL,NULL),(11,'JaEla',NULL,NULL),(12,'Katunayaka',NULL,NULL),(13,'Mirigama',NULL,NULL),(14,'Negomobo',NULL,NULL);

/*Table structure for table `city_distances` */

DROP TABLE IF EXISTS `city_distances`;

CREATE TABLE `city_distances` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `distance_km` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `city_a_id` bigint(20) unsigned NOT NULL,
  `city_b_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `city_a_fk_4553981` (`city_a_id`),
  KEY `city_b_fk_4553982` (`city_b_id`),
  CONSTRAINT `city_a_fk_4553981` FOREIGN KEY (`city_a_id`) REFERENCES `cities` (`id`),
  CONSTRAINT `city_b_fk_4553982` FOREIGN KEY (`city_b_id`) REFERENCES `cities` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `city_distances` */

/*Table structure for table `companies` */

DROP TABLE IF EXISTS `companies`;

CREATE TABLE `companies` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `company_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `companies` */

insert  into `companies`(`id`,`company_name`,`created_at`,`updated_at`,`deleted_at`) values (1,'Sams & Sams (Pvt) Ltd.',NULL,NULL,NULL);

/*Table structure for table `company_wallets` */

DROP TABLE IF EXISTS `company_wallets`;

CREATE TABLE `company_wallets` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `amount` decimal(15,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `companies_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `companies_fk_4591774` (`companies_id`),
  CONSTRAINT `companies_fk_4591774` FOREIGN KEY (`companies_id`) REFERENCES `companies` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `company_wallets` */

insert  into `company_wallets`(`id`,`amount`,`created_at`,`updated_at`,`deleted_at`,`companies_id`) values (1,1300.00,'2021-08-12 00:36:40','2021-08-15 18:58:11',NULL,1);

/*Table structure for table `deliveries` */

DROP TABLE IF EXISTS `deliveries`;

CREATE TABLE `deliveries` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `seller_address` longtext COLLATE utf8mb4_unicode_ci,
  `buyer_address` longtext COLLATE utf8mb4_unicode_ci,
  `delivery_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `product_id` bigint(20) unsigned DEFAULT NULL,
  `seller_id` bigint(20) unsigned DEFAULT NULL,
  `buyer_id` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_fk_4565141` (`product_id`),
  KEY `seller_fk_4565142` (`seller_id`),
  KEY `buyer_fk_4565144` (`buyer_id`),
  CONSTRAINT `buyer_fk_4565144` FOREIGN KEY (`buyer_id`) REFERENCES `web_users` (`id`),
  CONSTRAINT `product_fk_4565141` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  CONSTRAINT `seller_fk_4565142` FOREIGN KEY (`seller_id`) REFERENCES `web_users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `deliveries` */

insert  into `deliveries`(`id`,`seller_address`,`buyer_address`,`delivery_status`,`created_at`,`updated_at`,`deleted_at`,`product_id`,`seller_id`,`buyer_id`) values (1,'asdasda','asdasda','1','2021-08-17 22:36:06','2021-08-17 22:36:09',NULL,5,14,14);

/*Table structure for table `media` */

DROP TABLE IF EXISTS `media`;

CREATE TABLE `media` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  `uuid` char(36) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `collection_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `conversions_disk` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` bigint(20) unsigned NOT NULL,
  `manipulations` json NOT NULL,
  `custom_properties` json NOT NULL,
  `responsive_images` json NOT NULL,
  `order_column` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `media_model_type_model_id_index` (`model_type`,`model_id`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `media` */

insert  into `media`(`id`,`model_type`,`model_id`,`uuid`,`collection_name`,`name`,`file_name`,`mime_type`,`disk`,`conversions_disk`,`size`,`manipulations`,`custom_properties`,`responsive_images`,`order_column`,`created_at`,`updated_at`) values (4,'App\\Models\\Product',1,'32fb47b8-8978-4a9f-996c-50424510ae7b','images','6117412eb9951_5e8b1253a9575_Mawbima Logo','6117412eb9951_5e8b1253a9575_Mawbima-Logo.jpg','image/jpeg','s3','s3',49952,'[]','{\"custom_headers\": {\"ACL\": \"public-read\"}, \"generated_conversions\": {\"thumb\": true, \"preview\": true}}','[]',3,'2021-08-14 04:06:08','2021-08-14 04:06:11'),(5,'App\\Models\\Product',1,'655c7d0b-439a-4315-b928-420ef5bafd1b','images','6117412f6033e_5e8c32e908546_mawbima-recovery','6117412f6033e_5e8c32e908546_mawbima-recovery.jpg','image/jpeg','s3','s3',78811,'[]','{\"custom_headers\": {\"ACL\": \"public-read\"}, \"generated_conversions\": {\"thumb\": true, \"preview\": true}}','[]',4,'2021-08-14 04:06:11','2021-08-14 04:06:13'),(6,'App\\Models\\Category',5,'b04701cc-6fbb-468d-8859-c3d026b3dbc1','image','611762df3bd13_jewellery','611762df3bd13_jewellery.png','image/png','s3','s3',12803,'[]','{\"custom_headers\": {\"ACL\": \"public-read\"}, \"generated_conversions\": {\"thumb\": true, \"preview\": true}}','[]',5,'2021-08-14 06:29:56','2021-08-14 06:29:59'),(7,'App\\Models\\Category',4,'191def89-35d6-4639-bad0-527e845861be','image','611762fc372b6_fashion','611762fc372b6_fashion.png','image/png','s3','s3',9881,'[]','{\"custom_headers\": {\"ACL\": \"public-read\"}, \"generated_conversions\": {\"thumb\": true, \"preview\": true}}','[]',6,'2021-08-14 06:30:25','2021-08-14 06:30:26'),(8,'App\\Models\\Category',3,'fa4932a0-c353-42ba-a815-eeb7121486f1','image','61176328e1ce3_furniture','61176328e1ce3_furniture.png','image/png','s3','s3',7722,'[]','{\"custom_headers\": {\"ACL\": \"public-read\"}, \"generated_conversions\": {\"thumb\": true, \"preview\": true}}','[]',7,'2021-08-14 06:31:07','2021-08-14 06:31:10'),(9,'App\\Models\\Category',2,'5dc98c93-d537-4456-8808-c5641c077d23','image','611768f9b2c42_books','611768f9b2c42_books.png','image/png','s3','s3',8651,'[]','{\"custom_headers\": {\"ACL\": \"public-read\"}, \"generated_conversions\": {\"thumb\": true, \"preview\": true}}','[]',8,'2021-08-14 06:55:55','2021-08-14 06:55:57'),(10,'App\\Models\\Category',1,'d0df524d-6448-4ecf-8943-ef53784ae414','image','6117690e98aef_electronic','6117690e98aef_electronic.png','image/png','s3','s3',6952,'[]','{\"custom_headers\": {\"ACL\": \"public-read\"}, \"generated_conversions\": {\"thumb\": true, \"preview\": true}}','[]',9,'2021-08-14 06:56:16','2021-08-14 06:56:18'),(11,'App\\Models\\Category',6,'688eb2a1-51b4-46fb-bd8a-bea8cc04028e','image','611769218a444_sports','611769218a444_sports.png','image/png','s3','s3',43956,'[]','{\"custom_headers\": {\"ACL\": \"public-read\"}, \"generated_conversions\": {\"thumb\": true, \"preview\": true}}','[]',10,'2021-08-14 06:56:35','2021-08-14 06:56:38'),(12,'App\\Models\\Category',7,'0e4fb2d1-8add-46d8-afc3-a9780b4d0872','image','6117693347f26_baby','6117693347f26_baby.png','image/png','s3','s3',48955,'[]','{\"custom_headers\": {\"ACL\": \"public-read\"}, \"generated_conversions\": {\"thumb\": true, \"preview\": true}}','[]',11,'2021-08-14 06:56:56','2021-08-14 06:56:58'),(13,'App\\Models\\Product',2,'655c7d0b-439a-4315-b928-420ef5bafd1b','images','6117412f6033e_5e8c32e908546_mawbima-recovery','6117412f6033e_5e8c32e908546_mawbima-recovery.jpg','image/jpeg','s3','s3',78811,'[]','{\"custom_headers\": {\"ACL\": \"public-read\"}, \"generated_conversions\": {\"thumb\": true, \"preview\": true}}','[]',4,'2021-08-14 04:06:11','2021-08-14 04:06:13'),(14,'App\\Models\\Product',3,'655c7d0b-439a-4315-b928-420ef5bafd1b','images','611762df3bd13_jewellery','611762df3bd13_jewellery.png','image/jpeg','s3','s3',78811,'[]','{\"custom_headers\": {\"ACL\": \"public-read\"}, \"generated_conversions\": {\"thumb\": true, \"preview\": true}}','[]',4,'2021-08-14 04:06:11','2021-08-14 04:06:13'),(21,'App\\Models\\Product',5,'5301866d-76e4-495d-9047-432de6e972e6','images','611bdee885c65_h1-product-5-500x620','611bdee885c65_h1-product-5-500x620.jpg','image/jpeg','s3','s3',39063,'[]','{\"custom_headers\": {\"ACL\": \"public-read\"}, \"generated_conversions\": {\"thumb\": true, \"preview\": true}}','[]',18,'2021-08-17 16:08:16','2021-08-17 16:08:18'),(22,'App\\Models\\Product',5,'450a6ac1-69b6-4d58-89a4-60934d41a195','images','611bdeea9e0ad_h1-product-5-gallery-2','611bdeea9e0ad_h1-product-5-gallery-2.jpg','image/jpeg','s3','s3',77637,'[]','{\"custom_headers\": {\"ACL\": \"public-read\"}, \"generated_conversions\": {\"thumb\": true, \"preview\": true}}','[]',19,'2021-08-17 16:08:18','2021-08-17 16:08:19'),(23,'App\\Models\\Product',5,'a5e9cc9a-c529-4f5f-a900-388de6d79771','images','611bdeeb1bf6e_h1-product-5-gallery-3','611bdeeb1bf6e_h1-product-5-gallery-3.jpg','image/jpeg','s3','s3',91696,'[]','{\"custom_headers\": {\"ACL\": \"public-read\"}, \"generated_conversions\": {\"thumb\": true, \"preview\": true}}','[]',20,'2021-08-17 16:08:19','2021-08-17 16:08:21'),(24,'App\\Models\\Product',6,'c13eb0c6-97c7-4d46-9486-c8168477141d','images','611bdf02de74a_h11-product-1-500x620','611bdf02de74a_h11-product-1-500x620.jpg','image/jpeg','s3','s3',18107,'[]','{\"custom_headers\": {\"ACL\": \"public-read\"}, \"generated_conversions\": {\"thumb\": true, \"preview\": true}}','[]',21,'2021-08-17 16:08:39','2021-08-17 16:08:40'),(25,'App\\Models\\Product',6,'9c8c59cc-f357-4d85-8ada-68fe7c83ee12','images','611bdf03502ed_h11-product-1-gallery-1','611bdf03502ed_h11-product-1-gallery-1.jpg','image/jpeg','s3','s3',98999,'[]','{\"custom_headers\": {\"ACL\": \"public-read\"}, \"generated_conversions\": {\"thumb\": true, \"preview\": true}}','[]',22,'2021-08-17 16:08:41','2021-08-17 16:08:42'),(26,'App\\Models\\Product',7,'b6b2a17f-0bb0-4016-b332-7227d0d17528','images','611bdf5642fa8_h10-product-11-500x620','611bdf5642fa8_h10-product-11-500x620.jpg','image/jpeg','s3','s3',13893,'[]','{\"custom_headers\": {\"ACL\": \"public-read\"}, \"generated_conversions\": {\"thumb\": true, \"preview\": true}}','[]',23,'2021-08-17 16:10:02','2021-08-17 16:10:04'),(27,'App\\Models\\Product',8,'291ca954-351e-4506-9df8-8fed836bbb4c','images','611c0baa60963_h6-product-4-500x620','611c0baa60963_h6-product-4-500x620.jpg','image/jpeg','s3','s3',53978,'[]','{\"custom_headers\": {\"ACL\": \"public-read\"}, \"generated_conversions\": {\"thumb\": true, \"preview\": true}}','[]',24,'2021-08-17 19:19:36','2021-08-17 19:19:39'),(28,'App\\Models\\Product',8,'2ace00a1-f9d8-4172-82a8-2eb8fab56a5b','images','611c0bab2c95c_h6-product-4-gallery-1','611c0bab2c95c_h6-product-4-gallery-1.jpg','image/jpeg','s3','s3',86847,'[]','{\"custom_headers\": {\"ACL\": \"public-read\"}, \"generated_conversions\": {\"thumb\": true, \"preview\": true}}','[]',25,'2021-08-17 19:19:39','2021-08-17 19:19:41'),(29,'App\\Models\\Product',8,'d06d01ce-9287-484f-ab98-bea69d80b487','images','611c0bab90f28_h6-product-4-gallery-2','611c0bab90f28_h6-product-4-gallery-2.jpg','image/jpeg','s3','s3',133920,'[]','{\"custom_headers\": {\"ACL\": \"public-read\"}, \"generated_conversions\": {\"thumb\": true, \"preview\": true}}','[]',26,'2021-08-17 19:19:41','2021-08-17 19:19:44'),(30,'App\\Models\\Product',8,'7bc48590-c660-4ff5-94ee-e9e4fc779b87','images','611c0bac0d7c1_h6-product-4-gallery-4','611c0bac0d7c1_h6-product-4-gallery-4.jpg','image/jpeg','s3','s3',67849,'[]','{\"custom_headers\": {\"ACL\": \"public-read\"}, \"generated_conversions\": {\"thumb\": true, \"preview\": true}}','[]',27,'2021-08-17 19:19:44','2021-08-17 19:19:46'),(31,'App\\Models\\Product',9,'1a8f7787-cc9a-4ad4-b3f6-1b36eea0aaea','images','611c0c64aa6ef_beatz-studio-3-wireless-bluetooth-headphones-white','611c0c64aa6ef_beatz-studio-3-wireless-bluetooth-headphones-white.jpg','image/jpeg','s3','s3',47115,'[]','{\"custom_headers\": {\"ACL\": \"public-read\"}, \"generated_conversions\": {\"thumb\": true, \"preview\": true}}','[]',28,'2021-08-17 19:22:26','2021-08-17 19:22:28'),(32,'App\\Models\\Product',10,'957413d6-bb8a-4ef0-a009-4a56097cf6b5','images','611c0cc37b68d_bank-oluvsen-portable-bluetooth-speaker-grey-mist','611c0cc37b68d_bank-oluvsen-portable-bluetooth-speaker-grey-mist.jpg','image/jpeg','s3','s3',103883,'[]','{\"custom_headers\": {\"ACL\": \"public-read\"}, \"generated_conversions\": {\"thumb\": true, \"preview\": true}}','[]',29,'2021-08-17 19:23:59','2021-08-17 19:24:05'),(33,'App\\Models\\Product',11,'0d16990c-d276-4955-98ff-f44b5a5a32bb','images','611c0d2c64aa4_xboks-one-s-1-month-game-pass-ultimate-bundle-1-tb','611c0d2c64aa4_xboks-one-s-1-month-game-pass-ultimate-bundle-1-tb.jpg','image/jpeg','s3','s3',54575,'[]','{\"custom_headers\": {\"ACL\": \"public-read\"}, \"generated_conversions\": {\"thumb\": true, \"preview\": true}}','[]',30,'2021-08-17 19:25:47','2021-08-17 19:25:50'),(34,'App\\Models\\Product',12,'d6897d6a-b2a3-402a-8785-e11543b756fa','images','611c0d9a90abb_goxtrame-4k-ultra-hd-action-camera-black','611c0d9a90abb_goxtrame-4k-ultra-hd-action-camera-black.jpg','image/jpeg','s3','s3',95153,'[]','{\"custom_headers\": {\"ACL\": \"public-read\"}, \"generated_conversions\": {\"thumb\": true, \"preview\": true}}','[]',31,'2021-08-17 19:27:37','2021-08-17 19:27:40'),(35,'App\\Models\\Product',13,'2a2e0834-b44a-47df-99bb-99584f7aa23a','images','611c0de0d9444_xboks-one-s-1-month-game-pass-ultimate-bundle-1-tb','611c0de0d9444_xboks-one-s-1-month-game-pass-ultimate-bundle-1-tb.jpg','image/jpeg','s3','s3',54575,'[]','{\"custom_headers\": {\"ACL\": \"public-read\"}, \"generated_conversions\": {\"thumb\": true, \"preview\": true}}','[]',32,'2021-08-17 19:28:43','2021-08-17 19:28:45'),(36,'App\\Models\\Product',14,'d5d6a833-72e2-412b-977e-5c10989efce5','images','611c116fad77b_h5-product-6','611c116fad77b_h5-product-6.jpg','image/jpeg','s3','s3',42771,'[]','{\"custom_headers\": {\"ACL\": \"public-read\"}, \"generated_conversions\": {\"thumb\": true, \"preview\": true}}','[]',33,'2021-08-17 19:43:57','2021-08-17 19:44:00'),(37,'App\\Models\\Product',15,'ba8eb985-9363-4f32-8188-902907ef9b59','images','611c11ac64ebe_h5-product-7','611c11ac64ebe_h5-product-7.jpg','image/jpeg','s3','s3',19555,'[]','{\"custom_headers\": {\"ACL\": \"public-read\"}, \"generated_conversions\": {\"thumb\": true, \"preview\": true}}','[]',34,'2021-08-17 19:44:54','2021-08-17 19:44:56'),(38,'App\\Models\\Product',16,'58d2dc81-936a-48d6-b3f6-30884dbd91c3','images','611c133d9f2bf_h5-product-13','611c133d9f2bf_h5-product-13.jpg','image/jpeg','s3','s3',14194,'[]','{\"custom_headers\": {\"ACL\": \"public-read\"}, \"generated_conversions\": {\"thumb\": true, \"preview\": true}}','[]',35,'2021-08-17 19:51:43','2021-08-17 19:51:45'),(39,'App\\Models\\Product',17,'9e174524-397b-4373-bd79-58ee009b2084','images','611c13a5b7156_h8-product-2-500x620','611c13a5b7156_h8-product-2-500x620.jpg','image/jpeg','s3','s3',21180,'[]','{\"custom_headers\": {\"ACL\": \"public-read\"}, \"generated_conversions\": {\"thumb\": true, \"preview\": true}}','[]',36,'2021-08-17 19:53:18','2021-08-17 19:53:21'),(40,'App\\Models\\Product',18,'28d8cb7f-28b4-4917-a31b-9926b444215f','images','611c13ea6f6dc_h8-product-3-500x620','611c13ea6f6dc_h8-product-3-500x620.jpg','image/jpeg','s3','s3',64659,'[]','{\"custom_headers\": {\"ACL\": \"public-read\"}, \"generated_conversions\": {\"thumb\": true, \"preview\": true}}','[]',37,'2021-08-17 19:54:55','2021-08-17 19:54:57'),(41,'App\\Models\\Product',19,'3538e4a4-68e2-4950-bbe0-154af6784e52','images','611c1491986c3_Screenshot 2021-08-18 012654','611c1491986c3_Screenshot-2021-08-18-012654.png','image/png','s3','s3',125707,'[]','{\"custom_headers\": {\"ACL\": \"public-read\"}, \"generated_conversions\": {\"thumb\": true, \"preview\": true}}','[]',38,'2021-08-17 19:57:28','2021-08-17 19:57:31'),(42,'App\\Models\\Product',20,'ddb7fd71-0af3-4321-89ab-de51898656a6','images','611c15161594a_1','611c15161594a_1.jpg','image/jpeg','s3','s3',13770,'[]','{\"custom_headers\": {\"ACL\": \"public-read\"}, \"generated_conversions\": {\"thumb\": true, \"preview\": true}}','[]',39,'2021-08-17 19:59:50','2021-08-17 19:59:52'),(43,'App\\Models\\Product',21,'c4cafbbd-ef16-4bdb-b19c-a668bde93538','images','611c168830eb1_20_1','611c168830eb1_20_1.jpg','image/jpeg','s3','s3',31566,'[]','{\"custom_headers\": {\"ACL\": \"public-read\"}, \"generated_conversions\": {\"thumb\": true, \"preview\": true}}','[]',40,'2021-08-17 20:05:36','2021-08-17 20:05:39'),(44,'App\\Models\\Product',22,'aeee4532-d006-45f7-a2f9-56d30675bf30','images','611c16b88a17f_22_1','611c16b88a17f_22_1.jpg','image/jpeg','s3','s3',46404,'[]','{\"custom_headers\": {\"ACL\": \"public-read\"}, \"generated_conversions\": {\"thumb\": true, \"preview\": true}}','[]',41,'2021-08-17 20:06:24','2021-08-17 20:06:27'),(45,'App\\Models\\Product',23,'03b0ab73-9b33-4852-8657-2dd095a928ff','images','611c17190cb48_12_1','611c17190cb48_12_1.jpg','image/jpeg','s3','s3',46434,'[]','{\"custom_headers\": {\"ACL\": \"public-read\"}, \"generated_conversions\": {\"thumb\": true, \"preview\": true}}','[]',42,'2021-08-17 20:08:01','2021-08-17 20:08:03'),(46,'App\\Models\\Product',24,'fd6d2582-5f28-4f67-a460-8258076181a7','images','611c17f997b25_Image-51','611c17f997b25_Image-51.jpg','image/jpeg','s3','s3',171903,'[]','{\"custom_headers\": {\"ACL\": \"public-read\"}, \"generated_conversions\": {\"thumb\": true, \"preview\": true}}','[]',43,'2021-08-17 20:11:45','2021-08-17 20:11:48'),(47,'App\\Models\\Product',25,'a1af0afa-b0bf-4bdd-ba65-10d539aba3f9','images','611c183e07dc5_Image-59','611c183e07dc5_Image-59.jpg','image/jpeg','s3','s3',346770,'[]','{\"custom_headers\": {\"ACL\": \"public-read\"}, \"generated_conversions\": {\"thumb\": true, \"preview\": true}}','[]',44,'2021-08-17 20:13:02','2021-08-17 20:13:06'),(48,'App\\Models\\Product',26,'922fcff5-e380-4fc2-854f-68f34fb48078','images','611c1884516e3_Image-62','611c1884516e3_Image-62.jpg','image/jpeg','s3','s3',147654,'[]','{\"custom_headers\": {\"ACL\": \"public-read\"}, \"generated_conversions\": {\"thumb\": true, \"preview\": true}}','[]',45,'2021-08-17 20:14:08','2021-08-17 20:14:11'),(49,'App\\Models\\Product',27,'a395648c-0800-4ac0-a118-0bf97759bdf9','images','611c18dcc849d_Image-29','611c18dcc849d_Image-29.jpg','image/jpeg','s3','s3',70340,'[]','{\"custom_headers\": {\"ACL\": \"public-read\"}, \"generated_conversions\": {\"thumb\": true, \"preview\": true}}','[]',46,'2021-08-17 20:15:37','2021-08-17 20:15:39'),(50,'App\\Models\\Product',28,'e6d4551a-2461-4d3a-b60c-7603a4b37669','images','611c1bffeb91c_brown-bear-printed-sweater','611c1bffeb91c_brown-bear-printed-sweater.jpg','image/jpeg','s3','s3',53580,'[]','{\"custom_headers\": {\"ACL\": \"public-read\"}, \"generated_conversions\": {\"thumb\": true, \"preview\": true}}','[]',47,'2021-08-17 20:28:55','2021-08-17 20:28:58'),(51,'App\\Models\\Product',30,'f45005b9-feab-4f5d-8919-6592b2f2e9d5','images','611c1d98755b9_ezgif.com-gif-maker (2)','611c1d98755b9_ezgif.com-gif-maker-(2).png','image/png','s3','s3',100106,'[]','{\"custom_headers\": {\"ACL\": \"public-read\"}, \"generated_conversions\": {\"thumb\": true, \"preview\": true}}','[]',48,'2021-08-17 20:35:46','2021-08-17 20:35:49'),(52,'App\\Models\\Product',31,'1a076832-5144-4187-bdf0-a268e6874051','images','611c1e01e4794_1628395303265_PicsArt_07-14-06.41.18_M (1)','611c1e01e4794_1628395303265_PicsArt_07-14-06.41.18_M-(1).jpg','image/webp','s3','s3',21134,'[]','{\"custom_headers\": {\"ACL\": \"public-read\"}}','[]',49,'2021-08-17 20:37:39','2021-08-17 20:37:39'),(53,'App\\Models\\Product',32,'a94b1ace-f874-48de-ba95-4eda0e61d549','images','611c1e41201d9_ezgif.com-gif-maker (3)','611c1e41201d9_ezgif.com-gif-maker-(3).png','image/png','s3','s3',107110,'[]','{\"custom_headers\": {\"ACL\": \"public-read\"}, \"generated_conversions\": {\"thumb\": true, \"preview\": true}}','[]',50,'2021-08-17 20:38:27','2021-08-17 20:38:29'),(54,'App\\Models\\Product',29,'3b333ead-6efc-4f2b-9cce-f60906831b9e','images','611c1e6d649ce_ezgif.com-gif-maker','611c1e6d649ce_ezgif.com-gif-maker.png','image/png','s3','s3',74406,'[]','{\"custom_headers\": {\"ACL\": \"public-read\"}, \"generated_conversions\": {\"thumb\": true, \"preview\": true}}','[]',51,'2021-08-17 20:39:11','2021-08-17 20:39:13');

/*Table structure for table `membership_fees` */

DROP TABLE IF EXISTS `membership_fees`;

CREATE TABLE `membership_fees` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `transaction_type` varchar(255) NOT NULL,
  `amount` decimal(15,2) NOT NULL,
  `companies_id` bigint(20) unsigned NOT NULL,
  `web_users_id` bigint(20) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `companies_fk_4624159` (`companies_id`),
  KEY `web_users_fk_4624160` (`web_users_id`),
  CONSTRAINT `companies_fk_4624159` FOREIGN KEY (`companies_id`) REFERENCES `companies` (`id`),
  CONSTRAINT `web_users_fk_4624160` FOREIGN KEY (`web_users_id`) REFERENCES `web_users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `membership_fees` */

insert  into `membership_fees`(`id`,`transaction_type`,`amount`,`companies_id`,`web_users_id`,`created_at`,`updated_at`,`deleted_at`) values (5,'REG_FEE',250.00,1,14,'2021-08-15 18:58:11','2021-08-15 18:58:11',NULL),(6,'ANNUAL_FEE',400.00,1,14,'2021-08-15 18:58:11','2021-08-15 18:58:11',NULL);

/*Table structure for table `messages` */

DROP TABLE IF EXISTS `messages`;

CREATE TABLE `messages` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sender_id` bigint(20) unsigned NOT NULL,
  `receiver_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sender_fk_4554359` (`sender_id`),
  KEY `receiver_fk_4554360` (`receiver_id`),
  CONSTRAINT `receiver_fk_4554360` FOREIGN KEY (`receiver_id`) REFERENCES `web_users` (`id`),
  CONSTRAINT `sender_fk_4554359` FOREIGN KEY (`sender_id`) REFERENCES `web_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `messages` */

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values (1,'2014_10_12_100000_create_password_resets_table',1),(2,'2021_08_11_000001_create_media_table',1),(3,'2021_08_11_000002_create_categories_table',1),(4,'2021_08_11_000003_create_deliveries_table',1),(5,'2021_08_11_000004_create_company_wallets_table',1),(6,'2021_08_11_000005_create_messages_table',1),(7,'2021_08_11_000006_create_transactions_table',1),(8,'2021_08_11_000007_create_biddings_table',1),(9,'2021_08_11_000008_create_phy_inspections_table',1),(10,'2021_08_11_000009_create_seller_has_produtcs_table',1),(11,'2021_08_11_000010_create_wallets_table',1),(12,'2021_08_11_000011_create_product_views_table',1),(13,'2021_08_11_000012_create_permissions_table',1),(14,'2021_08_11_000013_create_products_table',1),(15,'2021_08_11_000014_create_companies_table',1),(16,'2021_08_11_000015_create_black_lists_table',1),(17,'2021_08_11_000016_create_cities_table',1),(18,'2021_08_11_000017_create_roles_table',1),(19,'2021_08_11_000018_create_users_table',1),(20,'2021_08_11_000019_create_web_users_table',1),(21,'2021_08_11_000020_create_web_user_roles_table',1),(22,'2021_08_11_000021_create_city_distances_table',1),(23,'2021_08_11_000022_create_system_meta_table',1),(24,'2021_08_11_000023_create_role_user_pivot_table',1),(25,'2021_08_11_000024_create_permission_role_pivot_table',1),(26,'2021_08_11_000025_add_relationship_fields_to_deliveries_table',1),(27,'2021_08_11_000026_add_relationship_fields_to_system_meta_table',1),(28,'2021_08_11_000027_add_relationship_fields_to_company_wallets_table',1),(29,'2021_08_11_000028_add_relationship_fields_to_messages_table',1),(30,'2021_08_11_000029_add_relationship_fields_to_transactions_table',1),(31,'2021_08_11_000030_add_relationship_fields_to_biddings_table',1),(32,'2021_08_11_000031_add_relationship_fields_to_product_views_table',1),(33,'2021_08_11_000032_add_relationship_fields_to_users_table',1),(34,'2021_08_11_000033_add_relationship_fields_to_phy_inspections_table',1),(35,'2021_08_11_000034_add_relationship_fields_to_black_lists_table',1),(36,'2021_08_11_000035_add_relationship_fields_to_seller_has_produtcs_table',1),(37,'2021_08_11_000036_add_relationship_fields_to_city_distances_table',1),(38,'2021_08_11_000037_add_relationship_fields_to_wallets_table',1),(39,'2021_08_11_000038_add_relationship_fields_to_web_users_table',1),(40,'2021_08_11_000039_add_relationship_fields_to_products_table',1);

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `permission_role` */

DROP TABLE IF EXISTS `permission_role`;

CREATE TABLE `permission_role` (
  `role_id` bigint(20) unsigned NOT NULL,
  `permission_id` bigint(20) unsigned NOT NULL,
  KEY `role_id_fk_4553644` (`role_id`),
  KEY `permission_id_fk_4553644` (`permission_id`),
  CONSTRAINT `permission_id_fk_4553644` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_id_fk_4553644` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `permission_role` */

insert  into `permission_role`(`role_id`,`permission_id`) values (1,1),(1,2),(1,3),(1,4),(1,5),(1,6),(1,7),(1,8),(1,9),(1,10),(1,11),(1,12),(1,13),(1,14),(1,15),(1,16),(1,17),(1,18),(1,19),(1,20),(1,21),(1,22),(1,23),(1,24),(1,25),(1,26),(1,27),(1,28),(1,29),(1,30),(1,31),(1,32),(1,33),(1,34),(1,35),(1,36),(1,37),(1,38),(1,39),(1,40),(1,41),(1,42),(1,43),(1,44),(1,45),(1,46),(1,47),(1,48),(1,49),(1,50),(1,51),(1,52),(1,53),(1,54),(1,55),(1,56),(1,57),(1,58),(1,59),(1,60),(1,61),(1,62),(1,63),(1,64),(1,65),(1,66),(1,67),(1,68),(1,69),(1,70),(1,71),(1,72),(1,73),(1,74),(1,75),(1,76),(1,77),(1,78),(1,79),(1,80),(1,81),(1,82),(1,83),(1,84),(1,85),(1,86),(1,87),(1,88),(1,89),(1,90),(1,91),(1,92),(1,93),(1,94),(1,95),(1,96),(1,97),(1,98),(1,99),(1,100),(1,101),(1,102),(1,103),(1,104),(1,105),(2,17),(2,18),(2,19),(2,20),(2,21),(2,22),(2,23),(2,24),(2,25),(2,26),(2,27),(2,28),(2,29),(2,30),(2,31),(2,32),(2,33),(2,34),(2,35),(2,36),(2,37),(2,38),(2,39),(2,40),(2,41),(2,42),(2,43),(2,44),(2,45),(2,46),(2,47),(2,48),(2,49),(2,50),(2,51),(2,52),(2,53),(2,54),(2,55),(2,56),(2,57),(2,58),(2,59),(2,60),(2,61),(2,62),(2,63),(2,64),(2,65),(2,66),(2,67),(2,68),(2,69),(2,70),(2,71),(2,72),(2,73),(2,74),(2,75),(2,76),(2,77),(2,78),(2,79),(2,80),(2,81),(2,82),(2,83),(2,84),(2,85),(2,86),(2,87),(2,88),(2,89),(2,90),(2,91),(2,92),(2,93),(2,94),(2,95),(2,96),(2,97),(2,98),(2,99),(2,100),(2,101),(2,102),(2,103),(2,104),(2,105);

/*Table structure for table `permissions` */

DROP TABLE IF EXISTS `permissions`;

CREATE TABLE `permissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `permissions` */

insert  into `permissions`(`id`,`title`,`created_at`,`updated_at`,`deleted_at`) values (1,'user_management_access',NULL,NULL,NULL),(2,'permission_create',NULL,NULL,NULL),(3,'permission_edit',NULL,NULL,NULL),(4,'permission_show',NULL,NULL,NULL),(5,'permission_delete',NULL,NULL,NULL),(6,'permission_access',NULL,NULL,NULL),(7,'role_create',NULL,NULL,NULL),(8,'role_edit',NULL,NULL,NULL),(9,'role_show',NULL,NULL,NULL),(10,'role_delete',NULL,NULL,NULL),(11,'role_access',NULL,NULL,NULL),(12,'user_create',NULL,NULL,NULL),(13,'user_edit',NULL,NULL,NULL),(14,'user_show',NULL,NULL,NULL),(15,'user_delete',NULL,NULL,NULL),(16,'user_access',NULL,NULL,NULL),(17,'web_user_role_create',NULL,NULL,NULL),(18,'web_user_role_edit',NULL,NULL,NULL),(19,'web_user_role_show',NULL,NULL,NULL),(20,'web_user_role_delete',NULL,NULL,NULL),(21,'web_user_role_access',NULL,NULL,NULL),(22,'web_user_management_access',NULL,NULL,NULL),(23,'web_user_create',NULL,NULL,NULL),(24,'web_user_edit',NULL,NULL,NULL),(25,'web_user_show',NULL,NULL,NULL),(26,'web_user_delete',NULL,NULL,NULL),(27,'web_user_access',NULL,NULL,NULL),(28,'city_create',NULL,NULL,NULL),(29,'city_edit',NULL,NULL,NULL),(30,'city_show',NULL,NULL,NULL),(31,'city_delete',NULL,NULL,NULL),(32,'city_access',NULL,NULL,NULL),(33,'cities_management_access',NULL,NULL,NULL),(34,'city_distance_create',NULL,NULL,NULL),(35,'city_distance_edit',NULL,NULL,NULL),(36,'city_distance_show',NULL,NULL,NULL),(37,'city_distance_delete',NULL,NULL,NULL),(38,'city_distance_access',NULL,NULL,NULL),(39,'system_metum_create',NULL,NULL,NULL),(40,'system_metum_edit',NULL,NULL,NULL),(41,'system_metum_show',NULL,NULL,NULL),(42,'system_metum_delete',NULL,NULL,NULL),(43,'system_metum_access',NULL,NULL,NULL),(44,'black_list_create',NULL,NULL,NULL),(45,'black_list_edit',NULL,NULL,NULL),(46,'black_list_show',NULL,NULL,NULL),(47,'black_list_delete',NULL,NULL,NULL),(48,'black_list_access',NULL,NULL,NULL),(49,'category_create',NULL,NULL,NULL),(50,'category_edit',NULL,NULL,NULL),(51,'category_show',NULL,NULL,NULL),(52,'category_delete',NULL,NULL,NULL),(53,'category_access',NULL,NULL,NULL),(54,'product_create',NULL,NULL,NULL),(55,'product_edit',NULL,NULL,NULL),(56,'product_show',NULL,NULL,NULL),(57,'product_delete',NULL,NULL,NULL),(58,'product_access',NULL,NULL,NULL),(59,'products_management_access',NULL,NULL,NULL),(60,'product_view_create',NULL,NULL,NULL),(61,'product_view_edit',NULL,NULL,NULL),(62,'product_view_show',NULL,NULL,NULL),(63,'product_view_delete',NULL,NULL,NULL),(64,'product_view_access',NULL,NULL,NULL),(65,'wallet_create',NULL,NULL,NULL),(66,'wallet_edit',NULL,NULL,NULL),(67,'wallet_show',NULL,NULL,NULL),(68,'wallet_delete',NULL,NULL,NULL),(69,'wallet_access',NULL,NULL,NULL),(70,'seller_has_produtc_create',NULL,NULL,NULL),(71,'seller_has_produtc_edit',NULL,NULL,NULL),(72,'seller_has_produtc_show',NULL,NULL,NULL),(73,'seller_has_produtc_delete',NULL,NULL,NULL),(74,'seller_has_produtc_access',NULL,NULL,NULL),(75,'phy_inspection_create',NULL,NULL,NULL),(76,'phy_inspection_edit',NULL,NULL,NULL),(77,'phy_inspection_show',NULL,NULL,NULL),(78,'phy_inspection_delete',NULL,NULL,NULL),(79,'phy_inspection_access',NULL,NULL,NULL),(80,'bidding_create',NULL,NULL,NULL),(81,'bidding_edit',NULL,NULL,NULL),(82,'bidding_show',NULL,NULL,NULL),(83,'bidding_delete',NULL,NULL,NULL),(84,'bidding_access',NULL,NULL,NULL),(85,'transaction_create',NULL,NULL,NULL),(86,'transaction_edit',NULL,NULL,NULL),(87,'transaction_show',NULL,NULL,NULL),(88,'transaction_delete',NULL,NULL,NULL),(89,'transaction_access',NULL,NULL,NULL),(90,'message_create',NULL,NULL,NULL),(91,'message_edit',NULL,NULL,NULL),(92,'message_show',NULL,NULL,NULL),(93,'message_delete',NULL,NULL,NULL),(94,'message_access',NULL,NULL,NULL),(95,'company_wallet_access',NULL,NULL,NULL),(96,'delivery_edit',NULL,NULL,NULL),(97,'delivery_show',NULL,NULL,NULL),(98,'delivery_delete',NULL,NULL,NULL),(99,'delivery_access',NULL,NULL,NULL),(100,'company_create',NULL,NULL,NULL),(101,'company_edit',NULL,NULL,NULL),(102,'company_show',NULL,NULL,NULL),(103,'company_delete',NULL,NULL,NULL),(104,'company_access',NULL,NULL,NULL),(105,'profile_password_edit',NULL,NULL,NULL);

/*Table structure for table `phy_inspections` */

DROP TABLE IF EXISTS `phy_inspections`;

CREATE TABLE `phy_inspections` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `inspection_datetime` datetime NOT NULL,
  `note` longtext COLLATE utf8mb4_unicode_ci,
  `agreed` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `web_users_id` bigint(20) unsigned NOT NULL,
  `products_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `web_users_fk_4554291` (`web_users_id`),
  KEY `products_fk_4554292` (`products_id`),
  CONSTRAINT `products_fk_4554292` FOREIGN KEY (`products_id`) REFERENCES `products` (`id`),
  CONSTRAINT `web_users_fk_4554291` FOREIGN KEY (`web_users_id`) REFERENCES `web_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `phy_inspections` */

/*Table structure for table `product_views` */

DROP TABLE IF EXISTS `product_views`;

CREATE TABLE `product_views` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `count` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `products_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `products_fk_4554141` (`products_id`),
  CONSTRAINT `products_fk_4554141` FOREIGN KEY (`products_id`) REFERENCES `products` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `product_views` */

insert  into `product_views`(`id`,`count`,`created_at`,`updated_at`,`products_id`) values (1,2,NULL,NULL,5),(2,20,NULL,NULL,14),(3,2,NULL,NULL,19),(4,5,NULL,NULL,6);

/*Table structure for table `products` */

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `product_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date_time` datetime NOT NULL,
  `closing_date_time` datetime NOT NULL,
  `minimum_price` decimal(15,2) NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `categories_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `categories_fk_4554109` (`categories_id`),
  CONSTRAINT `categories_fk_4554109` FOREIGN KEY (`categories_id`) REFERENCES `categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `products` */

insert  into `products`(`id`,`product_name`,`description`,`start_date_time`,`closing_date_time`,`minimum_price`,`status`,`created_at`,`updated_at`,`deleted_at`,`categories_id`) values (1,'Test product','<p>Test desc</p>','2021-08-14 09:35:47','2021-08-18 09:35:49',500.00,'1','2021-08-14 04:06:08','2021-08-17 16:12:00','2021-08-17 16:12:00',2),(2,'Test product 1','<p>Test desc</p>','2021-08-14 09:35:47','2021-08-19 09:35:49',500.00,'1','2021-08-14 04:06:08','2021-08-17 16:11:56','2021-08-17 16:11:56',4),(3,'Test product 2','<p>Test desc</p>','2021-08-14 09:35:47','2021-08-17 23:35:49',500.00,'1','2021-08-14 04:06:08','2021-08-17 16:11:53','2021-08-17 16:11:53',2),(4,'Test product 3','<p>Test desc</p>','2021-08-14 09:35:47','2021-08-14 09:35:49',500.00,'1','2021-08-14 04:06:08','2021-08-17 16:11:49','2021-08-17 16:11:49',3),(5,'Brown Ballerinas','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ut ullamcorper leo, eget euismod orci. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum ultricies aliquam.</p><figure class=\"table\"><table><tbody><tr><th>Weight</th><td>0.75 kg</td></tr><tr><th>Dimensions</th><td>60 x 35 x 2 cm</td></tr><tr><th>Colors</th><td>Brown, Yellow</td></tr><tr><th>Sizes</th><td>Extra Small (XS), Large (L), Medium (M), Small (S)</td></tr></tbody></table></figure>','2021-08-17 21:31:02','2021-08-31 21:31:07',1200.00,'1','2021-08-17 16:01:24','2021-08-17 16:08:12',NULL,4),(6,'Ash Color Backpack','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ut ullamcorper leo, eget euismod orci. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum ultricies.</p><figure class=\"table\"><table><tbody><tr><th>Weight</th><td>1.5 kg</td></tr><tr><th>Dimensions</th><td>40 x 25 x 4 cm</td></tr><tr><th>Colors</th><td>Black, Brown, Grey, White</td></tr><tr><th>Sizes</th><td>One Size</td></tr></tbody></table></figure>','2021-08-17 21:33:35','2021-09-03 21:33:37',800.00,'1','2021-08-17 16:03:50','2021-08-17 16:03:50',NULL,4),(7,'Aviator sunglasses','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ut ullamcorper leo, eget euismod orci. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum ultricies aliquam.</p>','2021-08-17 21:34:58','2021-08-20 21:35:00',1200.00,'1','2021-08-17 16:05:09','2021-08-17 16:10:01',NULL,4),(8,'Handmade Handbag','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ut ullamcorper leo, eget euismod orci. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum ultricies aliquam.</p>','2021-08-18 00:49:07','2021-09-05 00:49:12',900.00,'1','2021-08-17 19:19:36','2021-08-17 19:19:36',NULL,4),(9,'BEATZ Studio 3 Wireless Bluetooth Headphones – White','<p>Radical, preliminary thinking, social return on investment mobilize thought partnership strategize. Deep dive the resistance; co-create radical effective. Inspirational, contextualize; granular effective gender rights inclusion. Innovate problem-solvers; youth, move the needle social entrepreneurship. Scalable, think tank sustainable support move the needle challenges and opportunities. And leverage revolutionary, green space social return on investment strengthening infrastructure. Ideate strengthening infrastructure low-hanging fruit initiative invest. Issue outcomes, humanitarian relief, innovate the resistance.</p><p>&nbsp;</p><figure class=\"table\"><table><tbody><tr><th>Weight</th><td><i>1 kg</i></td></tr><tr><th>Dimensions</th><td><i>15 × 15 × 15 cm</i></td></tr><tr><th>Brand</th><td><i>Beats</i></td></tr><tr><th>Operating system</th><td><i>iOS 13</i></td></tr><tr><th>Processor</th><td><i>A13 Bionic chip</i></td></tr><tr><th>Screen resolution</th><td><i>HD 1792 x 828</i></td></tr><tr><th>Touchscreen</th><td><i>Yes</i></td></tr><tr><th>Camera</th><td><i>Main: Dual 12 MP / 12 MP</i></td></tr><tr><th>Internal storage</th><td><i>64 GB</i></td></tr><tr><th>Memory card supported</th><td><i>No</i></td></tr><tr><th>4G network</th><td><i>4G / 3G</i></td></tr><tr><th>Bluetooth</th><td><i>Bluetooth 5.0</i></td></tr><tr><th>Speakers</th><td><i>Stereo speakers</i></td></tr><tr><th>Internal memory</th><td><i>64 GB</i></td></tr></tbody></table></figure>','2021-08-18 00:52:13','2021-09-05 00:52:16',2500.00,'1','2021-08-17 19:22:26','2021-08-17 19:22:26',NULL,1),(10,'BANK & OLUVSEN Portable Bluetooth Speaker – Grey Mist','<p>Radical, preliminary thinking, social return on investment mobilize thought partnership strategize. Deep dive the resistance; co-create radical effective. Inspirational, contextualize; granular effective gender rights inclusion. Innovate problem-solvers; youth, move the needle social entrepreneurship. Scalable, think tank sustainable support move the needle challenges and opportunities. And leverage revolutionary, green space social return on investment strengthening infrastructure. Ideate strengthening infrastructure low-hanging fruit initiative invest. Issue outcomes, humanitarian relief, innovate the resistance.</p>','2021-08-18 00:53:48','2021-08-29 00:53:50',3500.00,'1','2021-08-17 19:23:59','2021-08-17 19:23:59',NULL,1),(11,'XBOKS One S Ultimate Bundle – 1 TB','<p>Radical, preliminary thinking, social return on investment mobilize thought partnership strategize. Deep dive the resistance; co-create radical effective. Inspirational, contextualize; granular effective gender rights inclusion. Innovate problem-solvers; youth, move the needle social entrepreneurship. Scalable, think tank sustainable support move the needle challenges and opportunities. And leverage revolutionary, green space social return on investment strengthening infrastructure. Ideate strengthening infrastructure low-hanging fruit initiative invest. Issue outcomes, humanitarian relief, innovate the resistance.</p>','2021-08-18 00:55:32','2021-08-29 00:55:35',9000.00,'1','2021-08-17 19:25:47','2021-08-17 19:25:47',NULL,1),(12,'GOXTRAME 4K Ultra HD Action Camera – Black','<p>Radical, preliminary thinking, social return on investment mobilize thought partnership strategize. Deep dive the resistance; co-create radical effective. Inspirational, contextualize; granular effective gender rights inclusion. Innovate problem-solvers; youth, move the needle social entrepreneurship. Scalable, think tank sustainable support move the needle challenges and opportunities. And leverage revolutionary, green space social return on investment strengthening infrastructure. Ideate strengthening infrastructure low-hanging fruit initiative invest. Issue outcomes, humanitarian relief, innovate the resistance.</p>','2021-08-18 00:57:23','2021-08-21 00:57:25',27000.00,'1','2021-08-17 19:27:37','2021-08-17 19:27:37',NULL,1),(13,'DEI Mini 2 Drone Fly More Combo – Space Grey','<ul><li>6.1″ HD Liquid Retina touchscreen</li><li>Dual 12 MP main cameras</li><li>12 MP front camera</li><li>Apple A13 Bionic processor</li><li>Accessories are sold separately</li></ul>','2021-08-18 00:58:33','2021-08-21 00:58:34',45000.00,'1','2021-08-17 19:28:43','2021-08-17 19:28:43',NULL,1),(14,'Round Eearing','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ut ullamcorper leo, eget euismod orci. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum ultricies aliquam.</p>','2021-08-18 01:13:44','2021-08-29 01:13:46',2300.00,'1','2021-08-17 19:43:57','2021-08-17 19:43:57',NULL,5),(15,'Purple stone ring','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ut ullamcorper leo, eget euismod orci. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum ultricies aliquam.</p>','2021-08-18 01:14:45','2021-08-29 01:14:46',1000.00,'1','2021-08-17 19:44:54','2021-08-17 19:44:54',NULL,5),(16,'Gold earing','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ut ullamcorper leo, eget euismod orci. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum ultricies aliquam.</p>','2021-08-18 01:21:33','2021-08-21 01:21:35',35000.00,'1','2021-08-17 19:51:43','2021-08-17 19:51:43',NULL,5),(17,'Flexible lamp','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ut ullamcorper leo, eget euismod orci. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum ultricies aliquam.</p>','2021-08-18 01:23:10','2021-08-22 01:23:13',25000.00,'1','2021-08-17 19:53:18','2021-08-17 19:53:18',NULL,3),(18,'Handmade ceramic plates','<p><br>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In ut ullamcorper leo, eget euismod orci. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum ultricies aliquam.</p>','2021-08-18 01:24:44','2021-09-05 01:24:46',3000.00,'1','2021-08-17 19:54:55','2021-08-17 19:54:55',NULL,3),(19,'Teapot','<p><strong>Detail</strong></p><p>Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum omnis voluptas assumenda</p>','2021-08-18 01:27:18','2021-08-27 01:27:19',3000.00,'1','2021-08-17 19:57:28','2021-08-17 19:57:28',NULL,3),(20,'Originals Kaval Windbr','<p>Lorem ipsum dolor sit amet consectetur adipisicing elit, sed do eiusmod tempor incididunt</p><p>ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commo consequat. Duis aute irure dolor in reprehend in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt</p><p>&nbsp;</p><ul><li>Weight 400 g</li><li>Dimensions10 x 10 x 15 cm</li><li>Materials 60% cotton, 40% polyester</li><li>Other Info American heirloom jean shorts pug seitan letterpress</li></ul>','2021-08-18 01:29:38','2021-08-29 01:29:40',25000.00,'1','2021-08-17 19:59:50','2021-08-17 19:59:50',NULL,3),(21,'THE SPORT HERITAGE HIP PACK SPORTS','<p>Sed lobortis elit nec lacus congue tristique. Sed nunc orci, imperdiet et accumsan ac, tempor ut ante. Fusce ac magna maximus, malesuada tellus sed, sodales ligula. Sed a justo vel erat mollis vulputate. Donec dolor justo, porta sit amet ultricies ut, pulvinar a metus. Cras ut odio non nunc elementum finibus eu id odio. Fusce sagittis magna nec eros efficitur, non dictum odio sodales. Aliquam eu aliquet libero, non consequat sapien. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec eu dignissim libero, at condimentum lorem. Mauris orci dolor, ultrices ac lorem id, facilisis scelerisque ipsum.</p><p>Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem. Cras suscipit, sapien in pellentesque hendrerit, dolor quam ornare nisl, vitae tempus nibh urna eget sem. Duis non interdum arcu, sit amet pellentesque odio. In sit amet aliquet augue.</p>','2021-08-18 01:35:28','2021-08-29 01:35:30',1000.00,'1','2021-08-17 20:05:36','2021-08-17 20:05:36',NULL,6),(22,'JORDAN ONE TAKE SHOE BAKETBALL BACK','<p>Sed lobortis elit nec lacus congue tristique. Sed nunc orci, imperdiet et accumsan ac, tempor ut ante. Fusce ac magna maximus, malesuada tellus sed, sodales ligula. Sed a justo vel erat mollis vulputate. Donec dolor justo, porta sit amet ultricies ut, pulvinar a metus. Cras ut odio non nunc elementum finibus eu id odio. Fusce sagittis magna nec eros efficitur, non dictum odio sodales. Aliquam eu aliquet libero, non consequat sapien. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec eu dignissim libero, at condimentum lorem. Mauris orci dolor, ultrices ac lorem id, facilisis scelerisque ipsum.</p><p>Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem. Cras suscipit, sapien in pellentesque hendrerit, dolor quam ornare nisl, vitae tempus nibh urna eget sem. Duis non interdum arcu, sit amet pellentesque odio. In sit amet aliquet augue.</p>','2021-08-18 01:36:17','2021-08-20 01:36:18',2500.00,'1','2021-08-17 20:06:24','2021-08-17 20:06:24',NULL,6),(23,'GRAPHIC TANK PRO DRI FIT SPORT CLASH','<p>Sed lobortis elit nec lacus congue tristique. Sed nunc orci, imperdiet et accumsan ac, tempor ut ante. Fusce ac magna maximus, malesuada tellus sed, sodales ligula. Sed a justo vel erat mollis vulputate. Donec dolor justo, porta sit amet ultricies ut, pulvinar a metus. Cras ut odio non nunc elementum finibus eu id odio. Fusce sagittis magna nec eros efficitur, non dictum odio sodales. Aliquam eu aliquet libero, non consequat sapien. Interdum et malesuada fames ac ante ipsum primis in faucibus. Donec eu dignissim libero, at condimentum lorem. Mauris orci dolor, ultrices ac lorem id, facilisis scelerisque ipsum.</p><figure class=\"table\"><table><tbody><tr><th><strong>Stand Up</strong></th><td>35″L x 24″W x 37-45″H(front to back wheel)</td></tr><tr><th><strong>Folded (w/o wheels)</strong></th><td>32.5″L x 18.5″W x 16.5″H</td></tr><tr><th><strong>Folded (w/ wheels)</strong></th><td>32.5″L x 24″W x 18.5″H</td></tr></tbody></table></figure>','2021-08-18 01:37:53','2021-09-05 01:37:55',2500.00,'1','2021-08-17 20:08:00','2021-08-17 20:08:00',NULL,6),(24,'Coding for Kids','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>','2021-08-18 01:41:38','2021-08-22 01:41:39',500.00,'1','2021-08-17 20:11:45','2021-08-17 20:11:45',NULL,2),(25,'Books of Blood','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>','2021-08-18 01:42:55','2021-08-29 01:42:56',650.00,'1','2021-08-17 20:13:02','2021-08-17 20:13:02',NULL,2),(26,'Life On Our Planet','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>','2021-08-18 01:43:56','2021-08-29 01:44:00',800.00,'1','2021-08-17 20:14:08','2021-08-17 20:14:08',NULL,2),(27,'Life and Times of Michela','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p><p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>','2021-08-18 01:45:25','2021-08-29 01:45:26',500.00,'1','2021-08-17 20:15:36','2021-08-17 20:15:36',NULL,2),(28,'Hummingbird printed sweater','<p>Studio Design\' PolyFaune collection features classic products with colorful patterns, inspired by the traditional japanese origamis. To wear with a chino or jeans. The sublimation textile printing process provides an exceptional color rendering and a color, guaranteed overtime.</p>','2021-08-18 01:58:48','2021-09-05 01:58:49',30000.00,'1','2021-08-17 20:28:55','2021-08-17 20:28:55',NULL,7),(29,'IRON MAN CUDDLY TOY','<p>These marvelous hero adventures bedtime buddy is soft and cuddly ! your little hero will have a blast playing with these rough and tumble friends during the day and feel safe at night bringing this Super pal to bed! .</p>','2021-08-18 02:01:19','2021-09-05 02:01:21',2100.00,'1','2021-08-17 20:31:43','2021-08-17 20:31:43',NULL,7),(30,'BABY TAVEL COT','<ul><li>Presently you will rest along with your Infant side by side fair slide down the door.</li><li>High-Quality Child Playpen with Side Slide Door.</li><li>Travel bunk comes with essential Mattress.</li><li>Made of 100% polyester.</li><li>For newborn up to 36 months or a maximal weight of 15kg.</li><li>BED Region: Length 103 cm x Width 67 cm.</li><li>Two lockable wheels make simpler portability whereas bed is in use.</li><li>It is prepared with: - detachable bassinet - detachable changing station - canopy shade with mosquito net and hanging delicate <a href=\"https://www.kapruka.com/srilanka_online_shopping.jsp?d=toys\">Toys</a>.</li><li>Very simple overlap instrument and comes with a carrying pack for easy travel.</li><li>Frontal section with zipper, sewn on the inverse side may be a convenient pocket for putting away items.</li><li>See-through security networks on the sides to assist keep an eye on your child.</li><li>A delicate cushioned board with adjusted edges.</li><li>Choices for an extra mattress.</li><li>6 Month mechanical Parts warranty.</li><li>For children from birth up to 36 months or a greatest weight of 15kg.</li></ul><p>&nbsp;</p>','2021-08-18 02:02:57','2021-08-28 02:04:38',20000.00,'1','2021-08-17 20:35:46','2021-08-17 20:35:46',NULL,7),(31,'BABY TRAVEL STOLLERS','<p>3-position leaning back situate with level position and full walled in area for newborns<br>&nbsp;One-hand, free-standing fold<br>&nbsp;Reversible handle<br>&nbsp;Congruous Aurora car situate for travel framework use<br>&nbsp;Parent plate with glass holder and secured capacity compartment<br>&nbsp;Child plate with two glass holders and detachable, dishwasher-safe liner<br>Flexible canopy with peek-a-boo window<br>&nbsp;Ergonomic, cushioned push-handle with three tallness positions<br>&nbsp;Front-wheel suspension<br>&nbsp;Front-wheel swivels with toe-tap locks<br>Toe-tap brakes<br>&nbsp;Expansive and helpful shopping wicker containerDimensions:<br>&nbsp;Folded (L x W x H): approx. 88 x 56 x 41 cm<br>&nbsp;Unfolded (L x W x H): approx. 94 x 52 x 100 cm<br>&nbsp;Stroller Weight: approx. 10.5 kg<br>&nbsp;Material- Textile raw material: 100% polyester<br>&nbsp;Frame: steel<br>Seat: polyester<br>&nbsp;Wheels: rubber tyres</p>','2021-08-18 02:07:29','2021-08-29 02:07:31',45000.00,'1','2021-08-17 20:37:39','2021-08-17 20:38:37','2021-08-17 20:38:37',7),(32,'BABY TRAVEL STOLLERS','<p>3-position leaning back situate with level position and full walled in area for newborns<br>&nbsp;One-hand, free-standing fold<br>&nbsp;Reversible handle<br>&nbsp;Congruous Aurora car situate for travel framework use<br>&nbsp;Parent plate with glass holder and secured capacity compartment<br>&nbsp;Child plate with two glass holders and detachable, dishwasher-safe liner<br>Flexible canopy with peek-a-boo window<br>&nbsp;Ergonomic, cushioned push-handle with three tallness positions<br>&nbsp;Front-wheel suspension<br>&nbsp;Front-wheel swivels with toe-tap locks<br>Toe-tap brakes<br>&nbsp;Expansive and helpful shopping wicker containerDimensions:<br>&nbsp;Folded (L x W x H): approx. 88 x 56 x 41 cm<br>&nbsp;Unfolded (L x W x H): approx. 94 x 52 x 100 cm<br>&nbsp;Stroller Weight: approx. 10.5 kg<br>&nbsp;Material- Textile raw material: 100% polyester<br>&nbsp;Frame: steel<br>Seat: polyester<br>&nbsp;Wheels: rubber tyres</p>','2021-08-18 02:07:29','2021-08-29 02:07:31',45000.00,'1','2021-08-17 20:38:27','2021-08-17 20:38:27',NULL,7);

/*Table structure for table `role_user` */

DROP TABLE IF EXISTS `role_user`;

CREATE TABLE `role_user` (
  `user_id` bigint(20) unsigned NOT NULL,
  `role_id` bigint(20) unsigned NOT NULL,
  KEY `user_id_fk_4553653` (`user_id`),
  KEY `role_id_fk_4553653` (`role_id`),
  CONSTRAINT `role_id_fk_4553653` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `user_id_fk_4553653` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `role_user` */

insert  into `role_user`(`user_id`,`role_id`) values (1,1);

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `roles` */

insert  into `roles`(`id`,`title`,`created_at`,`updated_at`,`deleted_at`) values (1,'Admin',NULL,NULL,NULL),(2,'User',NULL,NULL,NULL);

/*Table structure for table `seller_has_produtcs` */

DROP TABLE IF EXISTS `seller_has_produtcs`;

CREATE TABLE `seller_has_produtcs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `web_users_id` bigint(20) unsigned NOT NULL,
  `products_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `web_users_fk_4554264` (`web_users_id`),
  KEY `products_fk_4554265` (`products_id`),
  CONSTRAINT `products_fk_4554265` FOREIGN KEY (`products_id`) REFERENCES `products` (`id`),
  CONSTRAINT `web_users_fk_4554264` FOREIGN KEY (`web_users_id`) REFERENCES `web_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `seller_has_produtcs` */

/*Table structure for table `system_meta` */

DROP TABLE IF EXISTS `system_meta`;

CREATE TABLE `system_meta` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `meta_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_value` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `companies_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `system_meta_meta_name_unique` (`meta_name`),
  KEY `companies_fk_4591776` (`companies_id`),
  CONSTRAINT `companies_fk_4591776` FOREIGN KEY (`companies_id`) REFERENCES `companies` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `system_meta` */

insert  into `system_meta`(`id`,`meta_name`,`meta_value`,`created_at`,`updated_at`,`companies_id`) values (1,'ANNUAL_FEE','400',NULL,NULL,1),(2,'REG_FEE','250',NULL,NULL,1),(3,'DELIVERY_FEE_PER_KM','30',NULL,NULL,1);

/*Table structure for table `transactions` */

DROP TABLE IF EXISTS `transactions`;

CREATE TABLE `transactions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `seller_amount` decimal(15,2) NOT NULL,
  `company_commission` decimal(15,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `seller_id` bigint(20) unsigned NOT NULL,
  `biddings_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `seller_fk_4554351` (`seller_id`),
  KEY `biddings_fk_4554352` (`biddings_id`),
  CONSTRAINT `biddings_fk_4554352` FOREIGN KEY (`biddings_id`) REFERENCES `biddings` (`id`),
  CONSTRAINT `seller_fk_4554351` FOREIGN KEY (`seller_id`) REFERENCES `web_users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `transactions` */

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` datetime DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `companies_id` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `companies_fk_4590986` (`companies_id`),
  CONSTRAINT `companies_fk_4590986` FOREIGN KEY (`companies_id`) REFERENCES `companies` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`email_verified_at`,`password`,`remember_token`,`created_at`,`updated_at`,`deleted_at`,`companies_id`) values (1,'Admin','admin@admin.com',NULL,'$2y$10$k7/CkTc3y2AJuXqV.mN1GOvwM9cO/v4MpHDZhDIyHsnld34MeNPc.',NULL,NULL,NULL,NULL,1);

/*Table structure for table `wallets` */

DROP TABLE IF EXISTS `wallets`;

CREATE TABLE `wallets` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `amount` decimal(15,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `web_users_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `web_users_fk_4554148` (`web_users_id`),
  CONSTRAINT `web_users_fk_4554148` FOREIGN KEY (`web_users_id`) REFERENCES `web_users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `wallets` */

insert  into `wallets`(`id`,`amount`,`created_at`,`updated_at`,`web_users_id`) values (1,0.00,'2021-08-15 18:57:25','2021-08-15 18:57:25',14);

/*Table structure for table `web_user_roles` */

DROP TABLE IF EXISTS `web_user_roles`;

CREATE TABLE `web_user_roles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `web_user_roles` */

insert  into `web_user_roles`(`id`,`role_name`,`created_at`,`updated_at`) values (1,'BUYER',NULL,NULL),(2,'SELLER',NULL,NULL);

/*Table structure for table `web_users` */

DROP TABLE IF EXISTS `web_users`;

CREATE TABLE `web_users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nic` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` longtext COLLATE utf8mb4_unicode_ci,
  `contact` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` datetime DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `roles_id` bigint(20) unsigned NOT NULL,
  `cities_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `web_users_nic_unique` (`nic`),
  UNIQUE KEY `web_users_contact_unique` (`contact`),
  KEY `roles_fk_4553689` (`roles_id`),
  KEY `cities_fk_4623152` (`cities_id`),
  CONSTRAINT `cities_fk_4623152` FOREIGN KEY (`cities_id`) REFERENCES `cities` (`id`),
  CONSTRAINT `roles_fk_4553689` FOREIGN KEY (`roles_id`) REFERENCES `web_user_roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `web_users` */

insert  into `web_users`(`id`,`first_name`,`last_name`,`nic`,`address`,`contact`,`email`,`email_verified_at`,`password`,`remember_token`,`status`,`created_at`,`updated_at`,`deleted_at`,`roles_id`,`cities_id`) values (14,'Harsha','sampath','931472941V','No. 6/3, Mahara Nugegoda, Kadawatha','0710567167','harshsampath@gmail.com','2021-08-15 18:58:30','$2y$10$5r9JGB9W23wueGUhVWDl8O..x.N0od/oiaKECXcBIotLEp7KmnuFu',NULL,'1','2021-08-15 18:57:25','2021-08-15 18:58:30',NULL,1,13);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
