@extends('layouts.app')

@section('content')

<!--============= Hero Section Starts Here =============-->
<div class="hero-section">
    <div class="container">
        <ul class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li>
                <span>Sign Up</span>
            </li>
        </ul>
    </div>
    <div class="bg_img hero-bg bottom_center" data-background="{{ asset('assets/images/banner/hero-bg.png') }}"></div>
</div>
<!--============= Hero Section Ends Here =============-->


<!--============= Account Section Starts Here =============-->
<section class="account-section padding-bottom">
    <div class="container">
        <div class="account-wrapper mt--100 mt-lg--440">
            <div class="left-side">
                <div class="section-header mb-4">
                    <h2 class="title">Seller Registration</h2>
                    <p>Fill in the details below.</p>
                </div>
                @if(session()->has('message'))
                <div class="alert alert-success mb-3">
                    {{ session()->get('message') }}
                </div>
                @endif
                @if(session()->has('error'))
                <div class="alert alert-danger mb-3">
                    {{ session()->get('error') }}
                </div>
                @endif
                <form class="login-form" method="POST" action="{{ route('register') }}">
                    @csrf

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group mb-30 @error('first_name') is-invalid @enderror">
                                <label for="first_name"><i class="fa fa-user"></i></label>

                                <input id="first_name" type="text" name="first_name" placeholder="First Name"
                                    value="{{ old('first_name') }}" required autocomplete="first_name" autofocus>

                                @error('first_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group mb-30 @error('last_name') is-invalid @enderror">
                                <label for="last-name"><i class="fa fa-user"></i></label>

                                <input id="last_name" type="text" name="last_name" placeholder="Last Name"
                                    value="{{ old('last_name') }}" required autocomplete="last_name">

                                @error('last_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="form-group mb-30 @error('nic') is-invalid @enderror">
                        <label for="nic"><i class="fa fa-id-card"></i></label>

                        <input id="nic" type="text" name="nic" placeholder="NIC" value="{{ old('nic') }}" required
                            autocomplete="nic">

                        @error('nic')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group mb-30 @error('address') is-invalid @enderror">
                        <label for="address"><i class="fa fa-address-book"></i></label>

                        <input id="address" type="text" name="address" placeholder="Address"
                            value="{{ old('address') }}" required autocomplete="address">

                        @error('address')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group select mb-30 @error('cities_id') is-invalid @enderror">
                        <label for="cities"><i class="fas fa-map-marked-alt"></i></label>

                        <select class="form-control" name="cities_id" id="cities" required>
                            <option value="" selected disabled>Select a City</option>
                            @foreach($cities as $id => $name)
                                <option value="{{ $id }}" {{ old('cities_id') == $id ? 'selected' : '' }}>{{ $name }}</option>
                            @endforeach
                        </select>

                        @error('cities_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group mb-30 @error('contact') is-invalid @enderror">
                        <label for="contact"><i class="fa fa-phone"></i></label>

                        <input id="contact" type="text" name="contact" placeholder="Phone" value="{{ old('contact') }}"
                            required autocomplete="contact">

                        @error('contact')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group mb-30 @error('email') is-invalid @enderror">
                        <label for="email"><i class="far fa-envelope"></i></label>

                        <input id="email" type="email" placeholder="Email Address" name="email"
                            value="{{ old('email') }}" required autocomplete="email">

                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="form-group mb-30 @error('password') is-invalid @enderror">
                        <label for="password"><i class="fas fa-lock"></i></label>

                        <input id="password" type="password" name="password" placeholder="Password" required
                            autocomplete="new-password">
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                        <span class="pass-type"><i class="fas fa-eye"></i></span>
                    </div>
                    <div class="form-group mb-30">
                        <label for="password-confirm"><i class="fas fa-lock"></i></label>
                        <input id="password-confirm" type="password" placeholder="Confirm Password"
                            name="password_confirmation" required autocomplete="new-password">
                        <span class="pass-type"><i class="fas fa-eye"></i></span>
                    </div>
                    <div class="form-group mb-0">
                        <input type="hidden" name="roles_id" required value="2">
                        <button type="submit" class="custom-button">Sign Up</button>
                    </div>
                </form>
                <div class="container">
                    <div class="row justify-content-center mt-30">
                        <p>Already a Seller? <a href="{{url('/login')}}">Sign in</a> here.</p>
                    </div>
                </div>
            </div>
            <div class="right-side cl-white">
                <div class="section-header mb-0">
                    <h3 class="title mt-0">or Sign up as a Buyer.</h3>
                    <p>Go to Buyer Registration.</p>
                    <a href="{{url('/buyer-signup')}}" class="custom-button">Sign Up</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!--============= Account Section Ends Here =============-->

@endsection
