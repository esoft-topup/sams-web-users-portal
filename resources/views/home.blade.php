@extends('layouts.app')

@section('content')
<!--============= Banner Section Starts Here =============-->
<section class="banner-section bg_img" data-background="assets/images/banner/banner-bg-5.jpg?v=2">
    <div class="container">
        <div class="row align-items-center justify-content-between">
            <div class="col-lg-6 col-xl-6">
                <div class="banner-content cl-white">
                    <h5 class="cate">Next Generation Auction</h5>
                    <h1 class="title"><span class="d-xl-block">Find Your</span> Next Deal!</h1>
                    <p>
                        Online Auction is where everyone goes to shop, sell,and give, while discovering variety and
                        affordability.
                    </p>
                    <a href="#0" class="custom-button yellow btn-large">Get Started</a>
                </div>
            </div>
            <div class="d-none d-lg-block col-lg-6">
                <div class="banner-thumb-2">
                    <img src="assets/images/banner/home-banner.png" alt="banner">
                </div>
            </div>
        </div>
    </div>
</section>
<!--============= Banner Section Ends Here =============-->


<div class="browse-section ash-bg">
    <!--============= Hightlight Slider Section Starts Here =============-->
    <div class="browse-slider-section mt--140">
        <div class="container">
            <div class="section-header-2 cl-white mb-4">
                <div class="left">
                    <h6 class="title pl-0 w-100">Browse by Category</h6>
                </div>
                <div class="slider-nav">
                    <a href="#0" class="bro-prev"><i class="flaticon-left-arrow"></i></a>
                    <a href="#0" class="bro-next active"><i class="flaticon-right-arrow"></i></a>
                </div>
            </div>
            <div class="m--15">
                <div class="browse-slider owl-theme owl-carousel">
                    @foreach($categories as $key => $category)
                        @include('components.category_item', ['data'=> $category])
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <!--============= Hightlight Slider Section Ends Here =============-->

    <!--============= Car Auction Section Starts Here =============-->
    @include('components.3col_category_section', ['category'=> $product_categories[0]])
    <!--============= Car Auction Section Ends Here =============-->
</div>


<!--============= Jewelry Auction Section Starts Here =============-->
@include('components.3col_category_section', ['category'=> $product_categories[1]])
<!--============= Jewelry Auction Section Ends Here =============-->


<!--============= Call In Section Starts Here =============-->
<section class="call-in-section pt-lg-3 pt-2 pt-max-xl-0">
    <div class="container">
        <div class="call-wrapper cl-white">
            <div class="section-header">
                <h3 class="title mb-0">Register for Bidding Now!</h3>
            </div>
            <a href="{{ url('/buyer-signup') }}" class="custom-button white">Register</a>
        </div>
    </div>
</section>
<!--============= Call In Section Ends Here =============-->


<!--============= Watches Auction Section Starts Here =============-->
@include('components.3col_category_section', ['category'=> $product_categories[2]])
<!--============= Watches Auction Section Ends Here =============-->


<!--============= Popular Auction Section Starts Here =============-->
<section class="popular-auction pt-lg-5 pt-4 pos-rel">
    <div class="popular-bg bg_img"></div>
    <div class="container">
        <div class="section-header cl-white">
            <h2 class="title">Latest Successful Biddings</h2>
            <p>Bid and win great deals,Our auction process is simple, efficient, and transparent.</p>
        </div>
        <div class="popular-auction-wrapper">
            <div class="row justify-content-center mb-30-none">
                @foreach ($won_bids as $won_bids)
                    @include('components.home_popular_item', ['item'=> $won_bids])
                @endforeach
            </div>
        </div>
    </div>
</section>
<!--============= Popular Auction Section Ends Here =============-->

<!--============= How Section Starts Here =============-->
<section class="how-section padding-top">
    <div class="container">
        <div class="how-wrapper section-bg">
            <div class="section-header text-lg-left">
                <h2 class="title">How it works</h2>
                <p>Easy 3 steps to win</p>
            </div>
            <div class="row justify-content-center mb--40">
                <div class="col-md-6 col-lg-4">
                    <div class="how-item">
                        <div class="how-thumb">
                            <img src="assets/images/how/how1.png" alt="how">
                        </div>
                        <div class="how-content">
                            <h4 class="title">Sign Up</h4>
                            <p>Pay Registration & Annual Fee</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="how-item">
                        <div class="how-thumb">
                            <img src="assets/images/how/how2.png" alt="how">
                        </div>
                        <div class="how-content">
                            <h4 class="title">Bid</h4>
                            <p>Submit 20% Deposit When You Bidding</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="how-item">
                        <div class="how-thumb">
                            <img src="assets/images/how/how3.png" alt="how">
                        </div>
                        <div class="how-content">
                            <h4 class="title">Win</h4>
                            <p>Fun - Excitement - Great Deals</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--============= How Section Ends Here =============-->


<!--============= Client Section Starts Here =============-->
<section class="client-section padding-top padding-bottom">
    <div class="container">
        <div class="section-header">
            <h2 class="title">Don’t just take our word for it!</h2>
            <p>Our hard work is paying off. Great reviews from amazing customers.</p>
        </div>
        <div class="m--15">
            <div class="client-slider owl-theme owl-carousel">
                <div class="client-item">
                    <div class="client-content">
                        <p>I can't stop bidding! It's a great way to spend some time and I want everything on Sams
                            Auction.</p>
                    </div>
                    <div class="client-author">
                        <div class="thumb">
                            <a href="#0">
                                <img src="assets/images/client/client01.png" alt="client">
                            </a>
                        </div>
                        <div class="content">
                            <h6 class="title"><a href="#0">Alexis Moore</a></h6>
                            <div class="ratings">
                                <span><i class="fas fa-star"></i></span>
                                <span><i class="fas fa-star"></i></span>
                                <span><i class="fas fa-star"></i></span>
                                <span><i class="fas fa-star"></i></span>
                                <span><i class="fas fa-star"></i></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="client-item">
                    <div class="client-content">
                        <p>I came I saw I won. Love what I have won, and will try to win something else.</p>
                    </div>
                    <div class="client-author">
                        <div class="thumb">
                            <a href="#0">
                                <img src="assets/images/client/client02.png" alt="client">
                            </a>
                        </div>
                        <div class="content">
                            <h6 class="title"><a href="#0">Darin Griffin</a></h6>
                            <div class="ratings">
                                <span><i class="fas fa-star"></i></span>
                                <span><i class="fas fa-star"></i></span>
                                <span><i class="fas fa-star"></i></span>
                                <span><i class="fas fa-star"></i></span>
                                <span><i class="fas fa-star"></i></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="client-item">
                    <div class="client-content">
                        <p>This was my first time, but it was exciting. I will try it again. Thank you so much.</p>
                    </div>
                    <div class="client-author">
                        <div class="thumb">
                            <a href="#0">
                                <img src="assets/images/client/client03.png" alt="client">
                            </a>
                        </div>
                        <div class="content">
                            <h6 class="title"><a href="#0">Tom Powell</a></h6>
                            <div class="ratings">
                                <span><i class="fas fa-star"></i></span>
                                <span><i class="fas fa-star"></i></span>
                                <span><i class="fas fa-star"></i></span>
                                <span><i class="fas fa-star"></i></span>
                                <span><i class="fas fa-star"></i></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--============= Client Section Ends Here =============-->
@endsection

@push('scripts')
    <script>
        $(function () {
            setTimeout(() => {
                @if(session()->has('reg_fee_success'))
                    @if(auth()->user()->roles_id == 1)
                        {!! 'Swal.fire("Successfully Paid!", "Thank you for your payment. We wishing you for happy bidding", "success")' !!};
                    @else
                        {!! 'Swal.fire("Successfully Paid!", "Thank you for your payment. We wishing you for happy selling", "success")' !!};
                    @endif
                @endif
            }, 2000);
        });
    </script>
@endpush
