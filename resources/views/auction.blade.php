@extends('layouts.app')

@section('content')
<!--============= Hero Section Starts Here =============-->
<div class="hero-section style-2">
    <div class="container">
        <ul class="breadcrumb">
            <li>
                <a href="{{ url('/') }}">Home</a>
            </li>
            @if(isset($category))
            <li>
                <a href="{{ url('/auction') }}">Auctions</a>
            </li>
            <li>
                <span>{{$category->category_name}}</span>
            </li>
            @else
            <li><span>Auction</span></li>
            @endif
        </ul>
    </div>
    <div class="bg_img hero-bg bottom_center" data-background="{{ asset('assets/images/banner/hero-bg.png') }}"></div>
</div>
<!--============= Hero Section Ends Here =============-->


<!--============= Featured Auction Section Starts Here =============-->
<section class="featured-auction-section padding-bottom mt--240 mt-lg--440 pos-rel">
    <div class="container">
        <div class="section-header cl-white mw-100 left-style">
            <h3 class="title">Bid on These Featured Auctions!</h3>
        </div>
        <div class="row justify-content-center mb-30-none">
            @foreach ($featured_products as $product)
            @include('components.3col_bidding_item', ['product'=> $product])
            @endforeach
        </div>
    </div>
</section>
<!--============= Featured Auction Section Ends Here =============-->


<!--============= Product Auction Section Starts Here =============-->
<div class="product-auction padding-bottom">
    <div class="container">
        <div class="row mb--50">
            <div class="col-lg-4 mb-50">
                <div class="widget">
                    <h5 class="title">Filter by Price</h5>
                    <div class="widget-body">
                        <div id="slider-range"></div>
                        <div class="price-range">
                            <input type="hidden" id="min_price" value="{{$min_price}}">
                            <input type="hidden" id="max_price"
                                value="{{$max_price == 999999999999 ? 50000 : $max_price}}">
                            <label for="amount">Price :</label>
                            <input type="text" id="amount" readonly>
                        </div>
                    </div>
                    <div class="text-center mt-20">
                        <a href="#" id="price" class="custom-button">Filter</a>
                    </div>
                </div>

                <div class="widget">
                    <h5 class="title">Auction Categories</h5>
                    <div class="widget-body">
                        @foreach ($categories as $key => $cat_item)
                        <div
                            class="widget-form-group {{ isset($category) && ($category->id == $cat_item->id)? 'active' : '' }}">
                            <a
                                href="{{ isset($category) && ($category->id == $cat_item->id) ? '#' : url('auction/' . $cat_item->id) }}">{{ $cat_item->category_name }}</a>
                        </div>
                        @endforeach
                    </div>
                </div>

            </div>
            <div class="col-lg-8 mb-50">
                <div class="product-header mb-40 style-2">
                    <form class="product-search" method="GET" action="{{ 
                        '/auction' . (isset($category) ? '/' .$category->id : '') }}">
                        <input type="text" name="search" placeholder="Find by Name" value="{{$search}}">
                        <button type="submit"><i class="fas fa-search"></i></button>
                    </form>
                </div>
                <div class="row mb-30-none justify-content-center">
                    @foreach ($products as $product)
                    @include('components.3col_bidding_item', ['product'=> $product, 'columns' => 'col-lg-6'])
                    @endforeach
                </div>
                {{ $products->links('vendor.pagination.custom') }}
            </div>
        </div>
    </div>
</div>
<!--============= Product Auction Section Ends Here =============-->
@endsection