@extends('layouts.app')

@section('content')

<!--============= Hero Section Starts Here =============-->
<div class="hero-section">
    <div class="container">
        <ul class="breadcrumb">
            <li>
                <a href="#">Register</a>
            </li>
            <li>
                <span>Proceed Payment</span>
            </li>
        </ul>
    </div>
    <div class="bg_img hero-bg bottom_center" data-background="{{ asset('assets/images//banner/hero-bg.png') }}"></div>
</div>
<!--============= Hero Section Ends Here =============-->


<!--============= Account Section Starts Here =============-->
<section class="account-section padding-bottom">
    <div class="container">
        <div class="account-wrapper mt--100 mt-lg--440 align-items-center justify-content-center">
            <div class="p-5 text-center">
                <div class="section-header">
                    <h2 class="title">Regitration & Annual Payment</h2>
                </div>
                @if(session()->has('success'))
                <div class="alert alert-success mb-3">
                    {{ session()->get('success') }}
                </div>
                @endif
                @if(session()->has('message'))
                <div class="alert alert-warning mb-3">
                    {{ session()->get('message') }}
                </div>
                @endif
                @if(session()->has('error'))
                <div class="alert alert-warning mb-3">
                    {{ session()->get('error') }}
                </div>
                @endif
                <form method="post" action="https://sandbox.payhere.lk/pay/checkout">
                    <div class="row mb-4">
                        <div class="col-lg-12">
                            <h4 class="mt-0 mb-4">You will pay below fees in this step</h3>
                                <h5 class="mb-3">Reg. Fee: Rs. {{$reg_fee_meta->meta_value}}</h5>
                                <h5 class="">Annual Fee: Rs. {{$annual_fee_meta->meta_value}}</h5>
                        </div>
                    </div>
                    <div class="form-group mb-0">
                        <input type="hidden" name="merchant_id" value="1218349">
                        <input type="hidden" name="return_url" value="{{ url('/payment-response') }}">
                        <input type="hidden" name="cancel_url" value="{{ url('/payment-response') }}">
                        <input type="hidden" name="notify_url" value="{{ url('/payment-response') }}">
                        <input type="hidden" name="order_id" value="{{$transaction->id}}">
                        <input type="hidden" name="items" value="Sams Auction Registration & Annual Fee">
                        <input type="hidden" name="currency" value="LKR">
                        <input type="hidden" name="amount"
                            value="{{number_format((float) $reg_fee_meta->meta_value + (float) $annual_fee_meta->meta_value, 0, '', '')}}">
                        <input type="hidden" name="first_name" value="{{auth()->user()->first_name}}">
                        <input type="hidden" name="last_name" value="{{auth()->user()->last_name}}">
                        <input type="hidden" name="email" value="{{auth()->user()->email}}">
                        <input type="hidden" name="phone" value="{{auth()->user()->contact}}">
                        <input type="hidden" name="address" value="{{auth()->user()->address}}">
                        <input type="hidden" name="city" value="{{auth()->user()->city->city_name}}">
                        <input type="hidden" name="country" value="Sri Lanka">
                        <button type="submit" class="custom-button">Proceed Payment</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>
</section>
<!--============= Account Section Ends Here =============-->

@endsection
