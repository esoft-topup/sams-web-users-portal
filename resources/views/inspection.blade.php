@extends('layouts.app')

@section('content')

<!--============= Hero Section Starts Here =============-->
<div class="hero-section">
    <div class="container">
        <ul class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li>
                <a href="{{url('/seller-account')}}">Account</a>
            </li>
            <li>
                <span>Inspection</span>
            </li>
        </ul>
    </div>
    <div class="bg_img hero-bg bottom_center" data-background="{{ asset('assets/images/banner/hero-bg.png') }}"></div>
</div>
<!--============= Hero Section Ends Here =============-->


<!--============= Profile Section Starts Here =============-->
<section class="dashboard-section padding-bottom mt--240 mt-lg--440 pos-rel">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-3">
                @include('components.profile_side_view_seller', ['data'=> []])
            </div>
            <div class="col-9">
                <div class="dash-pro-item dashboard-widget">
                    <div class="header">
                        <h4 class="title">Manage Inspection Requests</h4>
                    </div>
                    <form class="login-form" method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="mb-20">
                            <p>Buyer Name</p>
                        </div>
                        <div class="custom-box mb-30 @error('webuser_id') is-invalid @enderror">

                            <input id="webuser_id" type="text" name="webuser_id" value="Andy" required readonly>

                            @error('webuser_id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            </select>
                        </div>
                        <div class="mb-20">
                            <p>Product Name</p>
                        </div>
                        <div class="custom-box mb-30 @error('product_id') is-invalid @enderror">

                            <input id="product_id" type="text" name="product_id" value="GOXTRAME 4K Ultra HD Action Camera – Black" required readonly> 

                            @error('product_id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="mb-20">
                            <p>Inspection Date Time</p>
                        </div>
                        <div class="custom-box mb-30 @error('inspect_date') is-invalid @enderror">

                            <input id="inspect_date" type="datetime-local" name="inspect_date" value="2021-08-19T10:00"required readonly>

                            @error('inspect_date')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="mb-20">
                            <p>Note</p>
                        </div>
                        <div class="custom-box mb-30 @error('note') is-invalid @enderror">

                            <textarea id="note" name="note" rows="2" required readonly>I want to see if the Camera captures low-light photos.</textarea>

                            @error('note')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="alert-widget">
                            <ul style="padding:0px 0px 20px;">
                                <li><input type="checkbox" id="yes" name="yes" value="1"><label for="yes"> Agree with Arrangement</label>
                                </li>
                            </ul>
                            @error('yes')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror

                        </div>

                        <div class="form-group mb-0">
                            <button type="submit" class="custom-button">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>

</section>
<!--============= Profile Section Ends Here =============-->

@endsection