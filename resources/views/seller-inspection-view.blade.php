@extends('layouts.app')

@section('content')

<!--============= Hero Section Starts Here =============-->
<div class="hero-section">
    <div class="container">
        <ul class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li>
                <a href="{{url('/seller-account')}}">Account</a>
            </li>
            <li>
                <span>Inspection</span>
            </li>
        </ul>
    </div>
    <div class="bg_img hero-bg bottom_center" data-background="{{ asset('assets/images/banner/hero-bg.png') }}"></div>
</div>
<!--============= Hero Section Ends Here =============-->


<!--============= Profile Section Starts Here =============-->
<section class="dashboard-section padding-bottom mt--240 mt-lg--440 pos-rel">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-3">
                @include('components.profile_side_view_seller', ['data'=> []])
            </div>
            <div class="col-lg-9">
                <div class="dashboard-widget">
                    <h5 class="title mb-30">Buyer Inspection Requests</h5>
                    <div class="dashboard-purchasing-tabs">
                        <table class="purchasing-table" style=text-align:center>
                            <thead>
                                <th>Req. ID</th>
                                <th>Buyer Name</th>
                                <th>Buyer Contact</th>
                                <th>Product Name</th>
                                <th>Inspection Date/Time</th>
                                <th width="190"></th>
                            </thead>
                            <tbody>
                                {{--@foreach ($biddings as $bidding)--}}
                                <tr>
                                    <td>15</td>
                                    <td>Andy</td>
                                    <td>0701005080</td>
                                    <td>GOXTRAME 4K Ultra HD Action Camera – Black</td>
                                    <td>2021-08-19 10:00:00</td>
                                    <td style=text-align:center><a href="{{url('/seller-inspection-view/15')}}" class="custom-button">View</a></td>
                                </tr>
                                {{--@endforeach--}}
                                <tr>
                                    <td>06</td>
                                    <td>Angela</td>
                                    <td>0720015001</td>
                                    <td>BANK & OLUVSEN Portable Bluetooth Speaker – Grey Mist</td>
                                    <td>2021-08-19 12:00:00</td>
                                    <td><a href="#0" class="custom-button">View</a></td>
                                </tr>
                                <tr>
                                    <td>05</td>
                                    <td>Dwight</td>
                                    <td>0711060010</td>
                                    <td>Purple stone ring</td>
                                    <td>2021-08-20 10:00:00</td>
                                    <td><a href="#0" class="custom-button">view</a></td>
                                </tr>
                                <tr>
                                    <td>03</td>
                                    <td>Andy</td>
                                    <td>0701500086</td>
                                    <td>Ash Color Backpack</td>
                                    <td>2021-08-19 08:00:00</td>
                                    <td><a href="#0" class="custom-button">View</a></td>
                                </tr>
                                <tr>
                                    <td>02</td>
                                    <td>Jim</td>
                                    <td>0701060010</td>
                                    <td>BANK & OLUVSEN Portable Bluetooth Speaker – Grey Mist</td>
                                    <td>2021-08-19 10:00:00</td>
                                    <td><a href="#0" class="custom-button">View</a></td>
                                </tr>
                                

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<!--============= Profile Section Ends Here =============-->

@endsection