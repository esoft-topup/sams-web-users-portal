@extends('layouts.app')

@section('content')

<!--============= Hero Section Starts Here =============-->
<div class="hero-section">
    <div class="container">
        <ul class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li>
                <a href="{{url('/buyer-account')}}">Account</a>
            </li>
            <li>
                <span>My Bids</span>
            </li>
        </ul>
    </div>
    <div class="bg_img hero-bg bottom_center" data-background="{{ asset('assets/images/banner/hero-bg.png') }}"></div>
</div>
<!--============= Hero Section Ends Here =============-->


<!--============= Profile Section Starts Here =============-->
<section class="dashboard-section padding-bottom mt--240 mt-lg--440 pos-rel">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-3">
                @include('components.profile_side_view_buyer', ['data'=> []])
            </div>
            <div class="col-lg-9">
                <div class="dash-bid-item dashboard-widget mb-40-60">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="header">
                                <h4 class="title">My Products</h4>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <a href="{{url('bid-create')}}" class="custom-button">Submit A Bid</a>
                        </div>
                    </div>
                </div>
                
                <div class="dashboard-widget">
                    <h5 class="title mb-30">Item Name : Baby Travel Stollers</h5>
                    <div class="dashboard-purchasing-tabs">
                        <table class="purchasing-table" style=text-align:center>
                            <thead>
                                <th>Buyer Name</th>
                                <th>Bid Value</th>
                                <th>Bid Date/Time</th>
                                <th></th>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>You</td>
                                    <td>2500.00</td>
                                    <td>2021-08-14 04:06:08</td>
                                    <td style=text-align:center><a href="0#" class="custom-button">Cancel A Bid</a></td>
                                </tr>
                                <tr>
                                    <td>Abhishek</td>
                                    <td>8000.00</td>
                                    <td>2021-08-14 04:10:02</td>
                                </tr>
                                <tr>
                                    <td>Gayathri</td>
                                    <td>3000.00</td>
                                    <td>2021-08-14 04:05:06</td>
                                </tr>
                                <tr>
                                    <td>Nipuna</td>
                                    <td>2500.00</td>
                                    <td>2021-08-14 04:02:00</td>
                                </tr>
                                <tr>
                                    <td>Andy</td>
                                    <td>900.00</td>
                                    <td>2021-08-14 05:06:10</td>
                                </tr>
                                <tr>
                                    <td>Angela</td>
                                    <td>800.00</td>
                                    <td>2021-08-14 05:10:06</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<!--============= Profile Section Ends Here =============-->

@endsection