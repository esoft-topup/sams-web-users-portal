@extends('layouts.app')

@section('content')

<!--============= Hero Section Starts Here =============-->
<div class="hero-section">
    <div class="container">
        <ul class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li>
                <a href="{{url('/buyer-account')}}">Account</a>
            </li>
            <li>
                <span>Profile</span>
            </li>
        </ul>
    </div>
    <div class="bg_img hero-bg bottom_center" data-background="{{ asset('assets/images/banner/hero-bg.png') }}"></div>
</div>
<!--============= Hero Section Ends Here =============-->


<!--============= Profile Section Starts Here =============-->
<section class="dashboard-section padding-bottom mt--240 mt-lg--440 pos-rel">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-3">
                @include('components.profile_side_view_buyer', ['data'=> []])
            </div>
            <div class="col-9">
                <div class="dash-pro-item mb-30 dashboard-widget">
                    <div class="header">
                        <h4 class="title">Personal Details</h4>
                    </div>

                    <form class="login-form" method="POST" action="{{ route('register') }}">
                        @csrf
                        <ul class="dash-pro-body">
                            <li>
                                <div class="info-name">First Name</div>
                                <div class="info-value @error('first_name') is-invalid @enderror">
                                    <input id="first_name" type="text" name="first_name" value="{{ old('first_name') }}" required autocomplete="first_name">

                                    @error('first_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </li>
                            <li>
                                <div class="info-name">Last Name</div>
                                <div class="info-value @error('last_name') is-invalid @enderror">
                                    <input id="last_name" type="text" name="last_name" value="{{ old('last_name') }}" required autocomplete="last_name">

                                    @error('last_name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </li>
                            <li>
                                <div class="info-name">NIC</div>
                                <div class="info-value @error('nic') is-invalid @enderror">
                                    <input id="nic" type="text" name="nic" value="{{ old('nic') }}" required autocomplete="nic">

                                    @error('nic')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </li>
                            <li>
                                <div class="info-name">Address</div>
                                <div class="info-value @error('address') is-invalid @enderror">
                                    <input id="address" type="text" name="address" value="{{ old('address') }}" required autocomplete="address">

                                    @error('address')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </li>
                            <li>
                                <div class="info-name">City</div>
                                <div class="info-value @error('cities_id') is-invalid @enderror">
                                    <select name="cities_id" id="cities" required>
                                        <option value="" selected disabled>Select a City</option>
                                        @foreach($cities as $id => $name)
                                        <option value="{{ $id }}">{{ $name }}</option>
                                        @endforeach

                                        @error('cities_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </select>
                                </div>
                            </li>
                            <li>
                                <div class="info-name">Phone</div>
                                <div class="info-value @error('contact') is-invalid @enderror">
                                    <input id="contact" type="text" name="contact" value="{{ old('contact') }}" required autocomplete="contact">

                                    @error('contact')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </li>
                        </ul>
                    </form>
                </div>


                <div class="dash-pro-item mb-30 dashboard-widget">
                    <div class="header">
                        <h4 class="title">Account Settings</h4>
                    </div>
                    <ul class="dash-pro-body">
                        <li>
                            <div class="info-name">Email</div>
                            <div class="info-value"></div>
                        </li>
                        <li>
                            <div class="info-name">Status</div>
                            <div class="info-value"><i class="flaticon-check text-success"></i> Active</div>
                        </li>
                    </ul>
                </div>



                <div class="dash-pro-item dashboard-widget">
                    <div class="header">
                        <h4 class="title">Security</h4>
                    </div>
                    <ul class="dash-pro-body">
                        <li>
                            <div class="info-name">Password</div>
                            <div class="info-value">******** <a href="">(Change Password)</a></div>
                        </li>
                    </ul>
                </div>
            </div>


        </div>


    </div>
    </div>
    </div>
</section>
<!--============= Profile Section Ends Here =============-->

@endsection