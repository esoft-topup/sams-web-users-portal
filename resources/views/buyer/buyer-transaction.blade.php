@extends('layouts.app')

@section('content')

<!--============= Hero Section Starts Here =============-->
<div class="hero-section">
    <div class="container">
        <ul class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li>
                <a href="{{url('/buyer-account')}}">Account</a>
            </li>
            <li>
                <span>Transactions</span>
            </li>
        </ul>
    </div>
    <div class="bg_img hero-bg bottom_center" data-background="{{ asset('assets/images/banner/hero-bg.png') }}"></div>
</div>
<!--============= Hero Section Ends Here =============-->


<!--============= Profile Section Starts Here =============-->
<section class="dashboard-section padding-bottom mt--240 mt-lg--440 pos-rel">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-3">
                @include('components.profile_side_view_buyer', ['data'=> []])
            </div>
            <div class="col-lg-9">
                <div class="dashboard-widget mb-40">
                    <div class="dashboard-title mb-30">
                        <h5 class="title">Wallet Balance</h5>
                    </div>
                    <div class="row justify-content-center mb-30-none">
                        <div class="col-12">
                            <div class="dashboard-item">
                                <div class="thumb">
                                    <img src="{{asset('assets/images/dashboard/wallet.png')}}" width="100px" height="auto" alt="dashboard">
                                </div>
                                <div class="content">
                                    <h2 class="title"><span class="counter">39,750</span></h2>
                                    <h6 class="info">LKR</h6>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="dashboard-widget">
                    <h5 class="title mb-10">Transactions</h5>
                    <div class="dashboard-purchasing-tabs">
                        <div class="tab-content">
                            <div class="tab-pane show active fade" id="current">
                                <table class="purchasing-table">
                                    <thead>
                                        <th>Item</th>
                                        <th>Seller</th>
                                        <th>Product</th>
                                        <th>Highest Bid</th>
                                        <th>Delivery Status</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td data-purchase="item">Round Eearing</td>
                                            <td data-purchase="bid price">Abhishek</td>
                                            <td data-purchase="highest bid">Purple stone ring</td>
                                            <td data-purchase="highest bid">LKR 5000.00</td>
                                            <td data-purchase="status">Completed</td>
                                        </tr>
                                        <tr>
                                            <td data-purchase="item">Ash Color Backpack</td>
                                            <td data-purchase="bid price">Abhishek</td>
                                            <td data-purchase="highest bid">Purple stone ring</td>
                                            <td data-purchase="highest bid">LKR 1200.00</td>
                                            <td data-purchase="status">Pending</td>
                                        </tr>
                                        <tr>
                                            <td data-purchase="item">Brown Ballerinas</td>
                                            <td data-purchase="bid price">Abhishek</td>
                                            <td data-purchase="highest bid">Purple stone ring</td>
                                            <td data-purchase="my bid">LKR 1300.00</td>
                                            <td data-purchase="status">Pending</td>
                                        </tr>
                                        <tr>
                                            <td data-purchase="item">Purple stone ring</td>
                                            <td data-purchase="bid price">Abhishek</td>
                                            <td data-purchase="highest bid">Purple stone ringg</td>
                                            <td data-purchase="my bid">LKR 1500.00</td>
                                            <td data-purchase="status">Pending</td>
                                        </tr>
                                        <tr>
                                            <td data-purchase="item">Round Eearing</td>
                                            <td data-purchase="bid price">Abhishek</td>
                                            <td data-purchase="highest bid">Purple stone ring</td>
                                            <td data-purchase="my bid">LKR 2800.00</td>
                                            <td data-purchase="status">Completed</td>
                                        </tr>
                                        <tr>
                                            <td data-purchase="item">Teapot</td>
                                            <td data-purchase="bid price">Abhishek</td>
                                            <td data-purchase="highest bid">Purple stone ring</td>
                                            <td data-purchase="my bid">LKR 4000.00</td>
                                            <td data-purchase="status">Completed</td>
                                        </tr>
                                        <tr>
                                            <td data-purchase="item">Books of Blood</td>
                                            <td data-purchase="bid price">Abhishek</td>
                                            <td data-purchase="highest bid">Purple stone ring</td>
                                            <td data-purchase="my bid">LKR 750.00</td>
                                            <td data-purchase="status">Completed</td>
                                        </tr>
                                        <tr>
                                            <td data-purchase="item">Handmade Handbag</td>
                                            <td data-purchase="bid price">Abhishek</td>
                                            <td data-purchase="highest bid">Purple stone ring</td>
                                            <td data-purchase="my bid">LKR 1200.00</td>
                                            <td data-purchase="status">Pending</td>
                                        </tr>
                                        <tr>
                                            <td data-purchase="item">BABY TAVEL COT</td>
                                            <td data-purchase="bid price">Abhishek</td>
                                            <td data-purchase="highest bid">Purple stone ring</td>
                                            <td data-purchase="my bid">LKR 22,000.00</td>
                                            <td data-purchase="status">Pending</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
        </section>
<!--============= Profile Section Ends Here =============-->

@endsection