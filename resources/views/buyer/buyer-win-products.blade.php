@extends('layouts.app')

@section('content')

<!--============= Hero Section Starts Here =============-->
<div class="hero-section">
    <div class="container">
        <ul class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li>
                <a href="{{url('/buyer-account')}}">Account</a>
            </li>
            <li>
                <span>Open Bids</span>
            </li>
        </ul>
    </div>
    <div class="bg_img hero-bg bottom_center" data-background="{{ asset('assets/images/banner/hero-bg.png') }}"></div>
</div>
<!--============= Hero Section Ends Here =============-->


<!--============= Profile Section Starts Here =============-->
<section class="dashboard-section padding-bottom mt--240 mt-lg--325 pos-rel">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-10 col-md-7 col-lg-3">
                @include('components.profile_side_view_buyer', ['data'=> []])
            </div>
            <div class="col-lg-9">
                <div class="dash-bid-item dashboard-widget mb-40-60">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="header">
                                <h4 class="title">My Products</h4>
                                <h5 class=""></h5>
                                <th>If the payment has not completed withing 24hr, seller has authotity to select the next highest bidder.</th>
                            </div>
                        </div>
                    </div>
                </div>

    <div class="row mb-30-none justify-content-center">
        <div class="col-sm-10 col-md-6">
            <div class="auction-item-2">
                <div class="auction-thumb">
                    <a href="product-details.html"><img src="assets/images/auction/product/04.png" alt="car"></a>
                    <a href="#0" class="rating"><i class="far fa-star"></i></a>
                    <a href="#0" class="bid"><i class="flaticon-auction"></i></a>
                </div>
                <div class="auction-content">
                    <h6 class="title">
                        <a href="#0">Ash Color Backpack</a>
                    </h6>
                    <div class="bid-area">
                        <div class="bid-amount">
                            <div class="icon">
                                <i class="flaticon-auction"></i>
                            </div>
                            <div class="amount-content">
                                <div class="current">Your Bid</div>
                                <div class="amount">LKR 1,200,000.00</div>
                            </div>
                        </div>
                    </div>
                    <div class="countdown-area">
                        <div class="countdown">
                            <div id="bid_counter26"></div>
                        </div>
                        <span class="total-bids">20 Bids</span>
                    </div>
                    <div class="text-center">
                        <td style=text-align:center><a href="{{url('/buyer-pay-products/15')}}" class="custom-button">Pay</a></td>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-10 col-md-6">
            <div class="auction-item-2">
                <div class="auction-thumb">
                    <a href="product-details.html"><img src="assets/images/auction/product/05.png" alt="car"></a>
                    <a href="#0" class="rating"><i class="far fa-star"></i></a>
                    <a href="#0" class="bid"><i class="flaticon-auction"></i></a>
                </div>
                <div class="auction-content">
                    <h6 class="title">
                        <a href="#0">BABY TAVEL COT</a>
                    </h6>
                    <div class="bid-area">
                        <div class="bid-amount">
                            <div class="icon">
                                <i class="flaticon-auction"></i>
                            </div>
                            <div class="amount-content">
                                <div class="current">Your Bid</div>
                                <div class="amount">LKR 876,000.00</div>
                            </div>
                        </div>
                    </div>
                    <div class="countdown-area">
                        <div class="countdown">
                            <div id="bid_counter27"></div>
                        </div>
                        <span class="total-bids">30 Bids</span>
                    </div>
                    <div class="text-center">
                        <td style=text-align:center><a href="{{url('/buyer-pay-products')}}" class="custom-button">Pay</a></td>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-10 col-md-6">
            <div class="auction-item-2">
                <div class="auction-thumb">
                    <a href="product-details.html"><img src="assets/images/auction/product/06.png" alt="product"></a>
                    <a href="#0" class="rating"><i class="far fa-star"></i></a>
                    <a href="#0" class="bid"><i class="flaticon-auction"></i></a>
                </div>
                <div class="auction-content">
                    <h6 class="title">
                        <a href="#0">Ash Color Backpack</a>
                    </h6>
                    <div class="bid-area">
                        <div class="bid-amount">
                            <div class="icon">
                                <i class="flaticon-auction"></i>
                            </div>
                            <div class="amount-content">
                                <div class="current">Your Bid</div>
                                <div class="amount">LKR 1000.00</div>
                            </div>
                        </div>
                    </div>
                    <div class="countdown-area">
                        <div class="countdown">
                            <div id="bid_counter1"></div>
                        </div>
                        <span class="total-bids">30 Bids</span>
                    </div>
                    <div class="text-center">
                        <a href="#0" class="custom-button">Pay</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-10 col-md-6">
            <div class="auction-item-2">
                <div class="auction-thumb">
                    <a href="product-details.html"><img src="assets/images/auction/product/09.png" alt="product"></a>
                    <a href="#0" class="rating"><i class="far fa-star"></i></a>
                    <a href="#0" class="bid"><i class="flaticon-auction"></i></a>
                </div>
                <div class="auction-content">
                    <h6 class="title">
                        <a href="#0">2017 Harley-Davidson XG750,</a>
                    </h6>
                    <div class="bid-area">
                        <div class="bid-amount">
                            <div class="icon">
                                <i class="flaticon-auction"></i>
                            </div>
                            <div class="amount-content">
                                <div class="current">Your Bid</div>
                                <div class="amount">LKR 450,000.00</div>
                            </div>
                        </div>
                    </div>
                    <div class="countdown-area">
                        <div class="countdown">
                            <div id="bid_counter2"></div>
                        </div>
                        <span class="total-bids">30 Bids</span>
                    </div>
                    <div class="text-center">
                        <a href="#0" class="custom-button">Pay</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</section>
<!--============= Profile Section Ends Here =============-->

@endsection