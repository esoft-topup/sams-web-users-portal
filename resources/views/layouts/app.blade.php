<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="shortcut icon" href="{{ asset('assets/images/sams/fevicon.png') }}" type="image/x-icon">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/nice-select.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/owl.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/flaticon.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/jquery-ui.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/css/styles.css') }}">

    {{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}
</head>

<body>
    <!--============= ScrollToTop Section Starts Here =============-->
    <div class="overlayer" id="overlayer">
        <div class="loader">
            <div class="loader-inner"></div>
        </div>
    </div>
    <a href="#0" class="scrollToTop"><i class="fas fa-angle-up"></i></a>
    <div class="overlay"></div>
    <!--============= ScrollToTop Section Ends Here =============-->


    <!--============= Header Section Starts Here =============-->
    <header>
        <div class="header-top">
            <div class="container">
                <div class="header-top-wrapper">
                    <ul class="customer-support">
                        <li>
                            <a href="#0" class="mr-3"><i class="fas fa-phone-alt"></i><span
                                    class="ml-2 d-none d-sm-inline-block">Customer Support</span></a>
                        </li>
                    </ul>
                    <ul class="cart-button-area">
                        @guest
                        <li class="pr-0">
                            <a href="{{ url('/seller-signup') }}" class="mr-3 text-white"></i>
                                <span class="ml-1 d-none d-sm-inline-block">Signup</span>
                            </a>
                        </li>
                        <li class="pr-0 pl-0">
                            <span class="mr-2 text-white d-none d-sm-inline-block">or</span>
                        </li>
                        <li class="pl-0">
                            <a href="{{ url('/login') }}" class="mr-3 text-white"></i>
                                <span class="ml-2 d-none d-sm-inline-block">Login</span>
                            </a>
                        </li>
                        @else
                        <li>
                            <a href="#" class="text-white">
                                <span class="ml-2 d-none d-sm-inline-block">Hi, {{auth()->user()->first_name}}</span>
                            </a>
                        </li>
                        <li>
                            <div class="dropdown profile">
                                <a href="#" class="user-button" id="dropdownMenuButton" data-toggle="dropdown"><i
                                        class="flaticon-user"></i></a>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                                    @if (auth()->user()->roles_id == 1)
                                        <a class="dropdown-item" href="{{ url('/buyer-account') }}">Profile</a>
                                        <a class="dropdown-item" href="{{ url('/buyer-products') }}">My Biddings</a>
                                        <a class="dropdown-item" href="{{ url('/buyer-transaction') }}">Transactions</a>
                                        <a class="dropdown-item" href="{{ url('/messages') }}">Messages</a>
                                    @else
                                        <a class="dropdown-item" href="{{ url('/seller-account') }}"">Profile</a>
                                        <a class="dropdown-item" href="{{ url('/seller-product') }}">My Products</a>
                                        <a class="dropdown-item" href="{{ url('/seller-transactions') }}">Transactions</a>
                                        <a class="dropdown-item" href="{{ url('/messages') }}">Messages</a>
                                    @endif
                                    <a class="dropdown-item" href="javascript:void"
                                        onclick="$('#logout-form').submit();">Logout</a>
                                </div>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                    style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </div>
        <div class="header-bottom">
            <div class="container">
                <div class="header-wrapper">
                    <div class="logo">
                        <a href="{{ url('/') }}">
                            <img src="{{ asset('assets/images/logo/logo.svg') }}" alt="Sams Auction Logo"
                                style="width:120px">
                        </a>
                    </div>
                    <ul class="menu ml-auto">
                        <li>
                            <a href="{{url('/')}}">Home</a>
                        </li>
                        <li>
                            <a href="{{url('/auction')}}">Auctions</a>
                        </li>
                        <li>
                            <a href="{{url('/about-us')}}">About Us</a>
                        </li>
                        <li>
                            <a href="{{url('/contact')}}">Contact</a>
                        </li>
                    </ul>
                    <form class="search-form" method="GET" action="{{ route('auction') }}">
                        <input type="text" name="search" placeholder="Search for auctions...." value="{{isset($search) ? $search : ''}}">
                        <button type="submit"><i class="fas fa-search"></i></button>
                    </form>
                    <div class="search-bar d-md-none">
                        <a href="#0"><i class="fas fa-search"></i></a>
                    </div>
                    <div class="header-bar d-lg-none">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!--============= Header Section Ends Here =============-->

    @yield('content')

    <!--============= Footer Section Starts Here =============-->
    <footer>
        <div class="footer-top pt-lg-5 pt-4 pb-lg-5 pb-4">
            <div class="container">
                <div class="row mb--60">
                    <div class="col-sm-6 col-lg-3">
                        <div class="footer-widget widget-links">
                            <h5 class="title">Auction Categories</h5>
                            <ul class="links-list">
                                @foreach($categories as $key => $category)
                                <li>
                                    <a href="#0">{{$category->category_name}}</a>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3">
                        <div class="footer-widget widget-links">
                            <h5 class="title">Pages</h5>
                            <ul class="links-list">
                                <li>
                                    <a href="#0">About Us</a>
                                </li>
                                <li>
                                    <a href="#0">FAQ</a>
                                </li>
                                <li>
                                    <a href="#0">Contact Us</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3">
                        <div class="footer-widget widget-links">
                            <h5 class="title">We're Here to Help</h5>
                            <ul class="links-list">
                                <li>
                                    <a href="#0">Your Account</a>
                                </li>
                                <li>
                                    <a href="#0">Safe and Secure</a>
                                </li>
                                <li>
                                    <a href="#0">Shipping Information</a>
                                </li>
                                <li>
                                    <a href="#0">Contact Us</a>
                                </li>
                                <li>
                                    <a href="#0">Help & FAQ</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3">
                        <div class="footer-widget widget-follow">
                            <h5 class="title">Follow Us</h5>
                            <ul class="links-list">
                                <li>
                                    <a href="#0"><i class="fas fa-phone-alt"></i>(646) 663-4575</a>
                                </li>
                                <li>
                                    <a href="#0"><i class="fas fa-blender-phone"></i>(646) 968-0608</a>
                                </li>
                                <li>
                                    <a href="#0"><i class="fas fa-envelope-open-text"></i><span class="__cf_email__"
                                            data-cfemail="147c71786454717a737b607c7179713a777b79">[email&#160;protected]</span></a>
                                </li>
                                <li>
                                    <a href="#0"><i class="fas fa-location-arrow"></i>1201 Broadway Suite</a>
                                </li>
                            </ul>
                            <ul class="social-icons">
                                <li>
                                    <a href="#0"><i class="fab fa-facebook-f"></i></a>
                                </li>
                                <li>
                                    <a href="#0"><i class="fab fa-twitter"></i></a>
                                </li>
                                <li>
                                    <a href="#0"><i class="fab fa-instagram"></i></a>
                                </li>
                                <li>
                                    <a href="#0"><i class="fab fa-linkedin-in"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="container">
                <div class="copyright-area">
                    <div class="footer-bottom-wrapper">
                        <div class="logo">
                            <a href="{{ url('/') }}"><img src="{{ asset('assets/images/logo/logo.svg') }}"
                                    alt="Sams Auction Logo" style="width:120px"></a>
                        </div>
                        <ul class="gateway-area">
                            <li>
                                <a href="#0"><img src="{{ asset('assets/images/footer/paypal.png') }}" alt="footer"></a>
                            </li>
                            <li>
                                <a href="#0"><img src="{{ asset('assets/images/footer/visa.png') }}" alt="footer"></a>
                            </li>
                            <li>
                                <a href="#0"><img src="{{ asset('assets/images/footer/discover.png') }}"
                                        alt="footer"></a>
                            </li>
                            <li>
                                <a href="#0"><img src="{{ asset('assets/images/footer/mastercard.png') }}"
                                        alt="footer"></a>
                            </li>
                        </ul>
                        <div class="copyright">
                            <p>&copy; Copyright 2021 | <a href="#0">Sams & Sams Auction</a> By <a href="#0">AlgoByte
                                    Solutions</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!--============= Footer Section Ends Here =============-->

    <script src="{{asset('assets/js/jquery-3.3.1.min.js') }}"></script>
    <script src="{{asset('assets/js/modernizr-3.6.0.min.js') }}"></script>
    <script src="{{asset('assets/js/plugins.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
        integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous">
    </script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="{{asset('assets/js/bootstrap.min.js') }}"></script>
    <script src="{{asset('assets/js/isotope.pkgd.min.js') }}"></script>
    <script src="{{asset('assets/js/wow.min.js') }}"></script>
    <script src="{{asset('assets/js/waypoints.js') }}"></script>
    <script src="{{asset('assets/js/nice-select.js') }}"></script>
    <script src="{{asset('assets/js/counterup.min.js') }}"></script>
    <script src="{{asset('assets/js/owl.min.js') }}"></script>
    <script src="{{asset('assets/js/magnific-popup.min.js') }}"></script>
    <script src="{{asset('assets/js/yscountdown.min.js') }}"></script>
    <script src="{{asset('assets/js/jquery-ui.min.js') }}"></script>
    <script src="{{asset('assets/js/main.js') }}"></script>

    <script src="{{asset('assets/js/scripts.js') }}"></script>
    @stack('scripts')
</body>

</html>
