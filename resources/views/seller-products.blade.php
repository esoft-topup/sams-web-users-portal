@extends('layouts.app')

@section('content')

<!--============= Hero Section Starts Here =============-->
<div class="hero-section">
    <div class="container">
        <ul class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li>
                <a href="{{url('/seller-account')}}">Account</a>
            </li>
            <li>
                <span>Open Bids</span>
            </li>
        </ul>
    </div>
    <div class="bg_img hero-bg bottom_center" data-background="{{ asset('assets/images/banner/hero-bg.png') }}"></div>
</div>
<!--============= Hero Section Ends Here =============-->


<!--============= Profile Section Starts Here =============-->
<section class="dashboard-section padding-bottom mt--240 mt-lg--440 pos-rel">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-3">
                @include('components.profile_side_view_seller', ['data'=> []])
            </div>
            <div class="col-lg-9">
                <div class="dash-bid-item dashboard-widget mb-40-60">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="header">
                                <h4 class="title">My Products</h4>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <a href="{{url('product-create')}}" class="custom-button">Create Product</a>
                        </div>
                    </div>
                </div>
                <div class="row mb-30-none justify-content-center">
                    @foreach ($products as $product)
                    @include('components.3col_bidding_item', ['product'=> $product, 'columns' => 'col-lg-6'])
                    @endforeach
                </div>
            </div>
        </div>
    </div>

</section>
<!--============= Profile Section Ends Here =============-->

@endsection