@extends('layouts.app')

@section('content')

<!--============= Hero Section Starts Here =============-->
<div class="hero-section">
    <div class="container">
        <ul class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li>
                <a href="{{url('/seller-account')}}">Account</a>
            </li>
            <li>
                <span>Transactions</span>
            </li>
        </ul>
    </div>
    <div class="bg_img hero-bg bottom_center" data-background="{{ asset('assets/images/banner/hero-bg.png') }}"></div>
</div>
<!--============= Hero Section Ends Here =============-->


<!--============= Profile Section Starts Here =============-->
<section class="dashboard-section padding-bottom mt--240 mt-lg--440 pos-rel">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-3">
                @include('components.profile_side_view_seller', ['data'=> []])
            </div>
            <div class="col-lg-9">
                <div class="dashboard-widget mb-40">
                    <div class="dashboard-title mb-30">
                        <h5 class="title">Wallet Balance</h5>
                    </div>
                    <div class="row justify-content-center mb-30-none">
                        <div class="col-12">
                            <div class="dashboard-item">
                                <div class="thumb">
                                    <img src="{{asset('assets/images/dashboard/wallet.png')}}" width="100px" height="auto" alt="dashboard">
                                </div>
                                <div class="content">
                                    <h2 class="title"><span class="counter">5,000</span></h2>
                                    <h6 class="info">LKR</h6>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="dashboard-widget">
                    <h5 class="title mb-30">Transactions</h5>
                    <div class="dashboard-purchasing-tabs">
                        <table class="purchasing-table" style=text-align:center>
                            <thead>
                                <th>Buyer Name</th>
                                <th>Product Name</th>
                                <th>Amount</th>
                                <th>Date/Time</th>
                                <th>Status</th>
                            </thead>
                            <tbody>
                                {{--@foreach ($biddings as $bidding)--}}
                                <tr>
                                    <td>Harsha</td>
                                    <td>BANK & OLUVSEN Portable Bluetooth Speaker – Grey Mist</td>
                                    <td>2000.00</td>
                                    <td>2021-08-14 04:06:08</td>
                                    <td>Paid</td>
                                </tr>
                                {{--@endforeach--}}
                                <tr>
                                    <td>Abhishek</td>
                                    <td>GOXTRAME 4K Ultra HD Action Camera – Black</td>
                                    <td>1200.00</td>
                                    <td>2021-08-14 04:10:02</td>
                                    <td>Not Paid</a></td>
                                </tr>
                                <tr>
                                    <td>Gayathri</td>
                                    <td>Purple stone ring</td>
                                    <td>1000.00</td>
                                    <td>2021-08-14 04:05:06</td>
                                    <td>Paid</td>
                                </tr>
                                <tr>
                                    <td>Nipuna</td>
                                    <td>Ash Color Backpack</td>
                                    <td>1050.00</td>
                                    <td>2021-08-14 04:02:00</td>
                                    <td>Paid</td>
                                </tr>
                                <tr>
                                    <td>Andy</td>
                                    <td>Purple stone ring</td>
                                    <td>900.00</td>
                                    <td>2021-08-14 05:06:10</td>
                                    <td>Paid</td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<!--============= Profile Section Ends Here =============-->

@endsection