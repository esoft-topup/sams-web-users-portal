@component('mail::message')
# Auction Registration

Kindly pay the registartion and annual fee before continue with sams & sams auction.

@component('mail::button', ['url' => url('/proceed-reg-payment/') ])
Click to Pay
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
