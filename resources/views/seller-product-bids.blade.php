@extends('layouts.app')

@section('content')

<!--============= Hero Section Starts Here =============-->
<div class="hero-section">
    <div class="container">
        <ul class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li>
                <a href="{{url('/seller-account')}}">Account</a>
            </li>
            <li>
                <span>My Products</span>
            </li>
        </ul>
    </div>
    <div class="bg_img hero-bg bottom_center" data-background="{{ asset('assets/images/banner/hero-bg.png') }}"></div>
</div>
<!--============= Hero Section Ends Here =============-->


<!--============= Profile Section Starts Here =============-->
<section class="dashboard-section padding-bottom mt--240 mt-lg--440 pos-rel">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-3">
                @include('components.profile_side_view_seller', ['data'=> []])
            </div>
            <div class="col-lg-9">
                <div class="dashboard-widget">
                    <h5 class="title mb-30">View Bids of Ash Colour Backpack</h5>
                    <div class="dashboard-purchasing-tabs">
                        <table class="purchasing-table">
                            <thead>
                                <th>Buyer Name</th>
                                <th>Bid Value</th>
                                <th>Bid Date/Time</th>
                                <th>Status</th>
                                <th width="190"></th>
                            </thead>
                            <tbody>
                                {{--@foreach ($biddings as $bidding)--}}
                                <tr>
                                    <td>Harsha</td>
                                    <td>2000.00</td>
                                    <td>2021-08-14 04:06:08</td>
                                    <td>Not Paid</td>
                                    <td></td>
                                </tr>
                                {{--@endforeach--}}
                                <tr>
                                    <td>Abhishek</td>
                                    <td>1200.00</td>
                                    <td>2021-08-14 04:10:02</td>
                                    <td>-</td>
                                    <td><a href="#0" class="custom-button">Declare Win</a></td>
                                </tr>
                                <tr>
                                    <td>Gayathri</td>
                                    <td>1000.00</td>
                                    <td>2021-08-14 04:05:06</td>
                                    <td>-</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Nipuna</td>
                                    <td>1050.00</td>
                                    <td>2021-08-14 04:02:00</td>
                                    <td>-</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Andy</td>
                                    <td>900.00</td>
                                    <td>2021-08-14 05:06:10</td>
                                    <td>-</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>Angela</td>
                                    <td>800.00</td>
                                    <td>2021-08-14 05:10:06</td>
                                    <td>-</td>
                                    <td></td>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>
<!--============= Profile Section Ends Here =============-->

@endsection