@extends('layouts.app')

@section('content')

<!--============= Hero Section Starts Here =============-->
<div class="hero-section style-2">
    <div class="container">
        <ul class="breadcrumb">
            <li>
                <a href="{{ url('/') }}">Home</a>
            </li>
            <li>
                <a href="{{ url('auction/') }}">Auctions</a>
            </li>
            <li>
                <span>{{ $product->product_name }}</span>
            </li>
        </ul>
    </div>
    <div class="bg_img hero-bg bottom_center" data-background="{{ asset('assets/images/banner/hero-bg.png') }}"></div>
</div>
<!--============= Hero Section Ends Here =============-->

<!--============= Product Details Section Starts Here =============-->
<section class="product-details padding-bottom mt--240 mt-lg--440">
    <div class="container">
        <div class="product-details-slider-top-wrapper">
            <div class="product-details-slider owl-theme owl-carousel" id="sync1">
                @foreach($product->images as $key => $media)
                <div class="slide-top-item">
                    <div class="slide-inner">
                        <img src="{{ $media->getUrl() }}" alt="product">
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        <div class="product-details-slider-wrapper">
            <div class="product-bottom-slider owl-theme owl-carousel" id="sync2">
                @foreach($product->images as $key => $media)
                <div class="slide-bottom-item">
                    <div class="slide-inner">
                        <img src="{{ $media->getUrl() }}" alt="product">
                    </div>
                </div>
                @endforeach
            </div>
            <span class="det-prev det-nav">
                <i class="fas fa-angle-left"></i>
            </span>
            <span class="det-next det-nav active">
                <i class="fas fa-angle-right"></i>
            </span>
        </div>
        <div class="row mt-40-60-80">
            <div class="col-lg-8">
                <div class="product-details-content">
                    <div class="product-details-header">
                        <h2 class="title">{{ $product->product_name }}</h2>
                        <ul>
                            {{-- <li>Listing ID: 14076242</li> --}}

                            <li>Item #: {{ $product->id }}</li>
                            <li>Seller: {{ $seller->first_name . ' ' . $seller->last_name }}</li>
                        </ul>
                    </div>
                    <ul class="price-table mb-30">
                        <li class="header">
                            <h5 class="current">Current Price</h5>
                            <h3 class="price">LKR {{ number_format($product->min_bid, 2, '.', ',') }}</h3>
                        </li>
                        <li>
                            <span class="details">Bidding Deposit</span>
                            <h5 class="info">20.00%</h5>
                        </li>
                        <li>
                            <span class="details">Bid Increment (LKR)</span>
                            <h5 class="info">LKR {{ number_format($other_info['min_increment'], 2, '.', ',') }}</h5>
                        </li>
                    </ul>
                    <div class="product-bid-area">
                        @if(session()->has('success'))
                        <div class="alert alert-success mb-3">
                            {{ session()->get('success') }}
                        </div>
                        @endif

                        @if(session()->has('error'))
                        <div class="alert alert-danger mb-3">
                            {{ session()->get('error') }}
                        </div>
                        @endif

                        @guest
                        <div class="alert alert-info">Please Login to Bid and Win!</div>
                        @else

                        @if($is_buyer)
                        @if(!$is_expired)
                        <form class="product-bid-form" method="POST" action="{{ route('bid-submit') }}">
                            <div class="search-icon">
                                <img src="{{ asset('assets/images/product/search-icon.png') }}" alt="product">
                            </div>
                            <div class="input-group bidding-amount-wrap">
                                <div class="input-group-prepend">
                                    <button class="btn btn-outline-secondary" type="button"
                                        onclick="decrement()">-</button>
                                </div>
                                <input type="number" readonly class="form-control" placeholder="Enter Your Bid"
                                    aria-label="Enter" aria-describedby="basic-addon2"
                                    min="{{ number_format($product->min_bid, 0, '', '') }}" name="demoInput"
                                    id="demoInput"
                                    value="{{ number_format($product->min_bid + $other_info['min_increment'], 0, '', '') }}">
                                <div class="input-group-append">
                                    <button class="btn btn-outline-secondary" type="button"
                                        onclick="increment()">+</button>
                                </div>
                            </div>
                            <script>
                                var increment_val = <?php echo $other_info['min_increment']?>;
                                var min_val = <?php echo number_format($product->min_bid, 0, '', '') ?>;
                                function increment() {
                                    if(document.getElementById('demoInput').value.length == 0){
                                        document.getElementById("demoInput").value  =  increment_val;
                                        return;
                                    }else{
                                        var val1 = document.getElementById('demoInput').value
                                        document.getElementById("demoInput").value  = parseInt(val1)  + increment_val;
                                    }
                                }

                                function decrement() {
                                    var val = document.getElementById('demoInput').value ;
                                    document.getElementById("demoInput").value  = parseInt(val)  - increment_val;
                                    if((parseInt(val)  - increment_val) < (min_val + increment_val)){
                                        document.getElementById("demoInput").value  = min_val + increment_val;
                                    }
                                }
                            </script>

                            <button type="submit" class="custom-button">Submit a bid</button>
                        </form>
                        @endif

                        @else
                            <div class="alert alert-info">You are not able to bid from seller account!</div>
                        @endif

                        @endguest
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="product-sidebar-area">
                    <div class="product-single-sidebar mb-3">
                        <h6 class="title">This Auction Ends in:</h6>
                        <div class="countdown">
                            <div class="auction_countdown" data-end="{{$product->end_date_time}}"></div>
                        </div>
                        <div class="side-counter-area">
                            <div class="side-counter-item">

                            </div>
                            <div class="side-counter-item">
                                <div class="thumb">
                                    <img src="{{ asset('assets/images/product/icon2.png') }}" alt="product') }}">
                                </div>
                                <div class="content">

                                    <h3 class="count-title"><span class="counter">{{$product_views->count}}</span></h3>
                                    <p>Views</p>
                                </div>
                            </div>
                            <div class="side-counter-item">
                                <div class="thumb">
                                    <img src="{{ asset('assets/images/product/icon3.png') }}" alt="product') }}">
                                </div>
                                <div class="content">
                                    <h3 class="count-title"><span
                                            class="counter">{{ isset($biddings) ? count($biddings) : 0 }}</span></h3>
                                    <p>Total Bids</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="product-tab-menu-area mb-40-60 mt-lg-5 mt-3">
        <div class="container">
            <ul class="product-tab-menu nav nav-tabs">
                <li>
                    <a href="#details" class="active" data-toggle="tab">
                        <div class="thumb">
                            <img src="{{ asset('assets/images/product/tab1.png') }}" alt="product">
                        </div>
                        <div class="content">Description</div>
                    </a>
                </li>
                <li>
                    <a href="#delevery" data-toggle="tab">
                        <div class="thumb">
                            <img src="{{ asset('assets/images/product/tab2.png') }}" alt="product">
                        </div>
                        <div class="content">Delivery Info</div>
                    </a>
                </li>
                <li>
                    <a href="#physical" data-toggle="tab">
                        <div class="thumb">
                            <img src="{{ asset('assets/images/overview/06.png') }}" alt="product">
                        </div>
                        <div class="content">Physical Inspection</div>
                    </a>
                </li>
                <li>
                    <a href="#history" data-toggle="tab">
                        <div class="thumb">
                            <img src="{{ asset('assets/images/product/tab3.png') }}" alt="product">
                        </div>
                        <div class="content">Bid History ({{isset($biddings) ? count($biddings) : 0}})</div>
                    </a>
                </li>
                <li>
                    <a href="#questions" data-toggle="tab">
                        <div class="thumb">
                            <img src="{{ asset('assets/images/product/tab4.png') }}" alt="product">
                        </div>
                        <div class="content">Questions </div>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="container">
        <div class="tab-content">
            <div class="tab-pane fade show active" id="details">
                <div class="tab-details-content">
                    <div class="header-area">
                        {!! $product->description !!}
                    </div>
                </div>
            </div>
            <div class="tab-pane fade show" id="delevery">
                <div class="shipping-wrapper">
                    <div class="item">
                        <h5 class="title">shipping</h5>
                        <div class="table-wrapper">
                            <table class="shipping-table mb-2">
                                <thead>
                                    <tr>
                                        <th>Available Delivery Methods: </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Standard Shipping (5-7 business days)</td>
                                    </tr>
                                </tbody>
                                <thead>
                                    <tr>
                                        <th>Seller Location:</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>{{ $other_info['seller_location'] }}</td>
                                    </tr>
                                </tbody>
                                <thead>
                                    <tr>
                                        <th>Courier Charge for Your Location:</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            @guest
                                            <div class="alert alert-info">Please Login to See the Courier Charge</div>
                                            @else
                                            {{ $other_info['delivery_fee'] }}
                                            @endguest
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="item">
                        <h5 class="title">Notes</h5>
                        <p>If you want to get more details about delivery charges please contact sams auction.</p>
                    </div>
                </div>
            </div>

            <div class="tab-pane fade show" id="physical">
                <div class="shipping-wrapper">
                    <div class="item">
                        <h5 class="title mb-4">Physical Inspection</h5>

                        @if(session()->has('success'))
                        <div class="alert alert-success mb-3">
                            {{ session()->get('success') }}
                        </div>
                        @endif

                        @guest
                        <div class="alert alert-info">Please Login to Submit a Physical Inspection Request</div>
                        @else

                        @if($is_buyer)
                        @if(!$is_expired)
                        <form method="POST" action="{{ route('physical-inspection-submit') }}">
                            @csrf
                            <div class="form-group mb-30 @error('inspection_datetime') is-invalid @enderror">
                                <label for="">Inspection Date & Time</label>
                                <input id="inspection_datetime" type="datetime-local" name="inspection_datetime"
                                    placeholder="" value="{{ date('Y-m-d\TH:i') }}" min="{{ date('Y-m-d\TH:i') }}"
                                    required>

                                @error('inspection_datetime')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group mb-30 @error('note') is-invalid @enderror">
                                <label for="">Note for Seller</label>
                                <textarea id="note" name="note" rows="1" required>{{ old('note') }}</textarea>

                                @error('note')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group mb-0">
                                <input type="hidden" name="products_id" value="{{$product->id}}">
                                <button type="submit" class="custom-button d-block ml-auto">Submit</button>
                            </div>
                        </form>
                        @endif
                        @else
                        <div class="alert alert-info">You are not able to make physical inspection request from seller account</div>
                        @endif
                        @endguest
                    </div>
                </div>
            </div>

            <div class="tab-pane fade show" id="history">
                <div class="history-wrapper">
                    <div class="item">
                        <h5 class="title">Bid History</h5>
                        <div class="history-table-area">
                            <table class="history-table">
                                <thead>
                                    <tr>
                                        <th>Bidder</th>
                                        <th>Date Time</th>
                                        <th>Bid Value</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(isset($biddings) && count($biddings) > 0)
                                    @foreach ($biddings as $bidding)
                                    <tr>
                                        <td data-history="bidder">
                                            <div class="user-info">
                                                <div class="content">
                                                    {{$bidding->bidder->first_name . ' ' . $bidding->bidder->last_name }}
                                                </div>
                                            </div>
                                        </td>
                                        <td data-history="date">{{ date_format($bidding->bidded_time,"Y/m/d h:i A") }}
                                        </td>
                                        <td data-history="unit price">LKR
                                            {{ number_format($bidding->bidded_value, 2, '.', ',') }}</td>
                                    </tr>
                                    @endforeach
                                    @else
                                    <tr>
                                        <td colspan="3">
                                            <div class="alert alert-info text-center">No Bids for This Product. Hurry
                                                Up!</div>
                                        </td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade show" id="questions">
                <h5 class="faq-head-title">Frequently Asked Questions</h5>
                <div class="faq-wrapper">
                    <div class="faq-item ">
                        <div class="faq-title">
                            <img src="{{ asset('assets/css/img/faq.png') }}" alt="css">
                            <span class="title">How to start bidding?</span>
                            <span class="right-icon">

                            </span>
                        </div>
                        <div class="faq-content" style="display: none;">
                            <p>All successful bidders can confirm their winning bid by checking the “Sams & Sams
                                Auction”. In addition, all successful bidders will receive an email notifying them of
                                their winning bid after the auction closes.</p>
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="faq-title">
                            <img src="{{ asset('assets/css/img/faq.png') }}" alt="css"><span class="title">Security
                                Deposit / Bidding Power </span><span class="right-icon"></span>
                        </div>
                        <div class="faq-content">
                            <p>All successful bidders can confirm their winning bid by checking the "Sams & Sams
                                Auction". In addition, all successful bidders will receive an email notifying them of
                                their winning bid after the auction closes.</p>
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="faq-title">
                            <img src="{{ asset('assets/css/img/faq.png') }}" alt="css"><span class="title">Delivery
                                time to the destination port </span><span class="right-icon"></span>
                        </div>
                        <div class="faq-content">
                            <p>All successful bidders can confirm their winning bid by checking the "Sams & Sams
                                Auction". In addition, all successful bidders will receive an email notifying them of
                                their winning bid after the auction closes.</p>
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="faq-title">
                            <img src="{{ asset('assets/css/img/faq.png') }}" alt="css"><span class="title">How to
                                register to bid in an auction?</span><span class="right-icon"></span>
                        </div>
                        <div class="faq-content">
                            <p>All successful bidders can confirm their winning bid by checking the "Sams & Sams
                                Auction". In addition, all successful bidders will receive an email notifying them of
                                their winning bid after the auction closes.</p>
                        </div>
                    </div>
                    <div class="faq-item open active">
                        <div class="faq-title">
                            <img src="{{ asset('assets/css/img/faq.png') }}" alt="css"><span class="title">How will I
                                know if my bid was successful?</span><span class="right-icon"></span>
                        </div>
                        <div class="faq-content">
                            <p>All successful bidders can confirm their winning bid by checking the "Sams & Sams
                                Auction". In addition, all successful bidders will receive an email notifying them of
                                their winning bid after the auction closes.</p>
                        </div>
                    </div>
                    <div class="faq-item">
                        <div class="faq-title">
                            <img src="{{ asset('assets/css/img/faq.png') }}" alt="css"><span class="title">What
                                happens if I bid on the wrong lot?</span><span class="right-icon"></span>
                        </div>
                        <div class="faq-content">
                            <p>All successful bidders can confirm their winning bid by checking the "Sams & Sams
                                Auction". In addition, all successful bidders will receive an email notifying them of
                                their winning bid after the auction closes.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
<!--============= Product Details Section Starts Here =============-->
@endsection