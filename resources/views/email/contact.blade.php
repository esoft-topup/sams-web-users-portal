<h3>Contact Mail From {{ $data['name'] }}</h3>

<table>
    <tbody>
    <tr>
        <td>Name</td>
        <td>{{ $data['name'] }}</td>
    </tr>
    <tr>
        <td>Email</td>
        <td>{{ $data['email'] }}</td>
    </tr>
    <tr>
        <td>Message</td>
        <td>{{ $data['message'] }}</td>
    </tr>
    </tbody>
</table>

<p>Thank you</p>