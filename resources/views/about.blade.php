@extends('layouts.app')

@section('content')
<!--============= Hero Section Starts Here =============-->
<div class="hero-section">
    <div class="container">
        <ul class="breadcrumb">
            <li>
                <a href="{{ url('/') }}">Home</a>
            </li>
            <li>
                <span>About Us</span>
            </li>
        </ul>
    </div>
    <div class="bg_img hero-bg bottom_center" data-background="assets/images/banner/hero-bg.png"></div>
</div>
<!--============= Hero Section Ends Here =============-->


<!--============= About Section Starts Here =============-->
<section class="contact-section padding-bottom">
    <div class="container">
        <div class="contact-wrapper padding-top padding-bottom mt--100 mt-lg--440S">
            <div class="section-header">
                <h3 class="Create">Who We Are</h3>
            <p style="color:black">Sam & Sam is a leading global digital marketplace that connects buyers and sellers. Work continues to focus on innovation using the latest technology. Sam & Sam offers unique opportunities for sellers and buyers.</p>

            </div>
        </div>
    </div>
</section>
<!--============= About Section Ends Here =============-->


<!--============= Counter Section Starts Here =============-->
<div class="counter-section padding-top mt--10">
    <div class="container">
        <div class="row justify-content-center mb-30-none">
            <div class="col-sm-6 col-lg-3">
                <div class="counter-item">
                    <h3 class="counter-header">
                        <span class="title counter">100</span><span class="title">K</span>
                    </h3>
                    <p>ITEMS AUCTIONED</p>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="counter-item">
                    <h3 class="counter-header">
                        <span>$</span><span class="title counter">90</span><span class="title">M</span>
                    </h3>
                    <p>IN SECURE BIDS</p>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="counter-item">
                    <h3 class="counter-header">
                        <span class="title counter">10</span><span class="title">K</span>
                    </h3>
                    <p>ITEMS AUCTIONED</p>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="counter-item">
                    <h3 class="counter-header">
                        <span>0</span><span class="title counter">5</span><span class="title">K</span>
                    </h3>
                    <p>AUCTION EXPERTS</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!--============= Counter Section Ends Here =============-->


<!--============= Overview Section Starts Here =============-->
<section class="overview-section padding-top">
    <div class="container mw-lg-100 p-lg-0">
        <div class="row m-0">
            <div class="col-lg-6 p-0">
                <div class="overview-content">
                    <div class="section-header text-lg-left">
                        <!--<h2 class="title">What can you expect?</h2>
                        <p>Voluptate aut blanditiis accusantium officiis expedita dolorem inventore odio reiciendis obcaecati quod nisi quae</p>-->
                    </div>
                    <div class="row mb--50">
                        <div class="col-sm-6">
                            <div class="expert-item">
                                <div class="thumb">
                                    <img src="assets/images/overview/01.png" alt="overview">
                                </div>
                                <div class="content">
                                    <h6 class="title">Real-time Auction</h6>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="expert-item">
                                <div class="thumb">
                                    <img src="assets/images/overview/delivery.png" alt="overview">
                                </div>
                                <div class="content">
                                    <h6 class="title">Delivery Options</h6>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="expert-item">
                                <div class="thumb">
                                    <img src="assets/images/overview/03.png" alt="overview">
                                </div>
                                <div class="content">
                                    <h6 class="title">Winner Announcement</h6>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="expert-item">
                                <div class="thumb">
                                    <img src="assets/images/overview/04.png" alt="overview">
                                </div>
                                <div class="content">
                                    <h6 class="title">Supports Multiple Currency</h6>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="expert-item">
                                <div class="thumb">
                                    <img src="assets/images/overview/05.png" alt="overview">
                                </div>
                                <div class="content">
                                    <h6 class="title">Show all bidders history</h6>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="expert-item">
                                <div class="thumb">
                                    <img src="assets/images/overview/06.png" alt="overview">
                                </div>
                                <div class="content">
                                    <h6 class="title">Add to watchlist</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 pl-30 pr-0">
                <div class="w-100 h-100 bg_img" data-background="{{ asset('assets/images/overview/bidding.png') }}"></div>
            </div>
        </div>
    </div>
</section>
<!--============= Overview Section Ends Here =============-->


<!--============= Call In Section Starts Here =============-->
<section class="call-in-section padding-top padding-bottom">
    <div class="container">
        <div class="row">
            <div class="col-md-6">

                <div class="call-wrapper cl-white bg_img" data-background="assets/images/call-in/call-bg.png">
                    <div class="section-header">
                        <h3 class="title">Vision</h3>
                        <p>To establish Sams & Sams Online Auction as Sri Lankan premier online auction company, making a global impact in the industry and embodying the values of honesty, integrity, and personal touch as the foundation of our business.</p>
                    </div>

                </div>
            </div>
            <div class="col-md-6">

                <div class="call-wrapper cl-white bg_img" data-background="assets/images/call-in/call-bg.png">
                    <div class="section-header">
                        <h3 class="title">Mission</h3>
                        <p>We bridge the gap between buyers and sellers to provide a seamless experience for all by upholding the highest standards of auctioneering and by matching the latest technology with the success.</p>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<!--============= Call In Section Ends Here =============-->



@endsection
