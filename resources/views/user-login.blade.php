@extends('layouts.app')

@section('content')

<!--============= Hero Section Starts Here =============-->
<div class="hero-section">
    <div class="container">
        <ul class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li>
                <span>Log In</span>
            </li>
        </ul>
    </div>
    <div class="bg_img hero-bg bottom_center" data-background="{{ asset('assets/images/banner/hero-bg.png') }}"></div>
</div>
<!--============= Hero Section Ends Here =============-->


<!--============= Account Section Starts Here =============-->
<section class="account-section padding-bottom">
    <div class="container">
        <div class="account-wrapper mt--100 mt-lg--440">
            <div class="left-side">
                <div class="section-header mb-4">
                    <h2 class="title">Log In</h2>
                    <p>Enter your Buyer/Seller account credentials below.</p>
                </div>
                @if(session()->has('success'))
                <div class="alert alert-success mb-3">
                    {{ session()->get('success') }}
                </div>
                @endif
                @if(session()->has('message'))
                <div class="alert alert-warning mb-3">
                    {{ session()->get('message') }}
                </div>
                @endif
                @if(session()->has('error'))
                <div class="alert alert-danger mb-3">
                    {{ session()->get('error') }}
                </div>
                @endif
                <form class="login-form" method="POST" action="{{ route('login') }}">
                    @csrf

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group mb-30 @error('email') is-invalid @enderror">
                                <label for="login-email"><i class="far fa-envelope"></i></label>
                                <input id="email" type="email" name="email" placeholder="Email Address"
                                    value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group @error('password') is-invalid @enderror">
                                <label for="login-pass"><i class="fas fa-lock"></i></label>
                                <input type="password" id="login-pass" placeholder="Password" name="password" required>
                                <span class="pass-type"><i class="fas fa-eye"></i></span>

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    {{-- <div class="form-group">
                        <a href="#0">Forgot Password?</a>
                    </div> --}}
                    <div class="form-group mb-0">
                        <button type="submit" class="custom-button">LOG IN</button>
                    </div>
                </form>
            </div>
            <div class="right-side cl-white">
                <div class="section-header mb-0">
                    <h3 class="title mt-0">Want to Register?</h3>
                </div>
                <div class="container">
                    <div class="row justify-content-center mb-30">
                        <p>Sign Up as a</p>
                    </div>
                    <div class="row justify-content-center mb-30"> <a href="{{url('/buyer-signup')}}"
                            class="custom-button">Buyer</a></div>
                    <div class="row justify-content-center mb-30">
                        <p>or</p>
                    </div>
                    <div class="row justify-content-center"> <a href="{{url('/seller-signup')}}"
                            class="custom-button">Seller</a></div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
<!--============= Account Section Ends Here =============-->

@endsection
