<section class="pt-lg-5 pt-4 pb-lg-5 pb-lg-4 pos-rel oh">
    <div class="container">
        <div class="section-header-3">
            <div class="left">
                <div class="thumb">
                    <img src="{{ isset($category) ? isset( $category->image) ? $category->image->getUrl(): ''  : '' }}" alt="header-icons">
                </div>
                <div class="title-area">
                    <h2 class="title">{{isset($category) ? $category->category_name : ''}}</h2>
                </div>
            </div>
            <a href="{{ isset($category) ? url('/auction/' . $category->id) : '' }}" class="normal-button">View
                All</a>
        </div>
        <div class="row justify-content-center mb-30-none">
            @if(isset($category->products))
            @foreach ($category->products as $product)
            @include('components.3col_bidding_item', ['product'=> $product])
            @endforeach
            @endif
        </div>
    </div>
</section>
