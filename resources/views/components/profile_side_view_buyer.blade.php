<div class="dashboard-widget mb-30 mb-lg-0 sticky-menu">
    <div class="user">
        <div class="thumb-area">
            <div class="thumb">
                <img src="{{ asset('assets/images/dashboard/user.png')}}" alt="user">
            </div>
        </div>
        <div class="content">
            <h5 class="title"><a href="#0">Gihani Rasanjana</a></h5>
        </div>
    </div>
    <ul class="profile-nav">
        <li>
            <a href="{{ url('/buyer-account') }}" class={{ isset($active_menu) && $active_menu == 2 ? 'active' : '' }}><i class="fa fa-user"></i>Profile </a>
        </li>
        <li>
            <a href="{{ url('/buyer-transaction') }}" class={{ isset($active_menu) && $active_menu == 1 ? 'active' : '' }}><i class="fa fa-money-check"></i>Transactions</a>
        </li>
        <li>
            <a href="{{ url('/buyer-products') }}" class={{ isset($active_menu) && $active_menu == 3 ? 'active' : '' }}><i class="fa fa-gavel"></i>My Bid Products</a>
        </li>
        <li>
            <a href="{{ url('/buyer-win-products') }}" class={{ isset($active_menu) && $active_menu == 4 ? 'active' : '' }}><i class="fa fa-check"></i>success Bids</a>
        </li>
        <li>
            <a href="{{ url('/buyer-inspection') }}" class={{ isset($active_menu) && $active_menu == 5 ? 'active' : '' }}><i class="fa fa-handshake"></i>Inspection Req.</a>
        </li>
        <li>
            <a href="notifications.html"><i class="fa fa-envelope"></i>Messages</a>
        </li>
    </ul>
</div>