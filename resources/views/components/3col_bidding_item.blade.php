<div class="col-sm-10 col-md-6 {{ isset($columns) ? $columns : 'col-lg-4' }}">
    <div class="auction-item-2">
        <div class="auction-thumb">
            <a href="{{ url('auction_view/'. $product->id) }}">
                <img src="{{ $product->image }}" alt="car">
            </a>
            {{--<a class={{ $product->product_name === 'BABY TAVEL COT' ? 'approved' : 'pending' }}>{{ $product->product_name === 'BABY TAVEL COT' ? 'Approved' : 'Pending' }}</a>--}}
        </div>
        <div class="auction-content">
            <h6 class="title">
                <a href="{{ url('auction_view/'. $product->id) }}">{{$product->product_name}}</a>
            </h6>
            <div class="bid-area">
                <div class="bid-amount">
                    <img class="icon" src="{{ asset('assets/images/sams/auction.png') }}" alt="">
                    <div class="amount-content">
                        <div class="current">Minimum Bid</div>
                        <div class="amount">{{ $product->minimum_price }}</div>
                    </div>
                </div>
            </div>
            <div class="countdown-area">
                <div class="countdown">
                    <div class="auction_countdown" data-end="{{$product->end_date_time}}"></div>
                </div>
                <span class="total-bids">{{ $product->bidding_count }} Bids</span>
            </div>
            <div class="text-center">
                <a href="{{ isset($button) && $button== 'view-bids' ? url('seller-product-bids/'. $product->id) : url('auction_view/'. $product->id) }}" class="custom-button">{{isset($button) && $button== 'view-bids' ? 'View Bids' : 'Submit a bid'}}</a>
            </div>
            
        </div>
    </div>
</div>
