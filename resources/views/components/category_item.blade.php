<a href="{{ url('auction/' . $data->id) }}" class="browse-item">
    @if($data->image)
        <img src="{{ $data->image->getUrl() }}" alt="{{ $data->category_name }}">
    @endif
    <span class="info">{{ $data->category_name }}</span>
</a>
