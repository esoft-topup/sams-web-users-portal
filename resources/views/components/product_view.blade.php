<a href="{{ url('auction_view/' . $data->id) }}" class="browse-item">
    @if($data->image)
        <img src="{{ $data->image->getUrl() }}" alt="{{ $data->product_name }}">
    @endif
    <span class="info">{{ $data->product_name }}</span>
</a>
