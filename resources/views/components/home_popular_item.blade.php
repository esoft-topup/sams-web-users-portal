<div class="col-lg-6">
    <div class="auction-item-3">
        <div class="auction-thumb">
            <a href="#">
                <img src="{{ $item->product->image }}" alt="popular">
            </a>
        </div>
        <div class="auction-content">
            <h6 class="title">
                <a href="#">{{ $item->product->product_name }}</a>
            </h6>
            <div class="bid-amount">
                <img  class="icon" src="{{ asset('assets/images/sams/winner.png') }}" alt="">
                <div class="amount-content">
                    <div class="current">Success Bid</div>
                    <div class="amount">{{ $item->product->won_price }}</div>
                </div>
            </div>
            <div class="bids-area">
                Total Bids : <span class="total-bids">{{ $item->product->bidding_count }} Bids</span>
            </div>
            <div class="bids-area">
                Winner : <span class="winner">{{ $item->product->winner }}</span>
            </div>
        </div>
    </div>
</div>
