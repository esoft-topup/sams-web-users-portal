<div class="dashboard-widget mb-30 mb-lg-0 sticky-menu">
    <div class="user">
        <div class="thumb-area">
            <div class="thumb">
                <img src="{{ asset('assets/images/dashboard/user.png')}}" alt="user">
            </div>
        </div>
        <div class="content">
            <h5 class="title"><a href="#0">Andy Bernard</a></h5>
        </div>
    </div>
    <ul class="profile-nav">
        <li>
            <a href="{{ url('/seller-transactions') }}" class={{ isset($active_menu) && $active_menu == 1 ? 'active' : '' }}><i class="fa fa-money-check"></i>Transactions</a>
        </li>
        <li>
            <a href="{{ url('/seller-account') }}" class={{ isset($active_menu) && $active_menu == 2 ? 'active' : '' }}><i class="fa fa-user"></i>Profile </a>
        </li>
        <li>
            <a href="{{ url('/seller-products') }}" class={{ isset($active_menu) && $active_menu == 3 ? 'active' : '' }}><i class="fa fa-gavel"></i>My Products</a>
        </li>
        
        <li>
            <a href="{{ url('/seller-inspection') }}" class={{ isset($active_menu) && $active_menu == 5 ? 'active' : '' }}><i class="fa fa-handshake"></i>Inspection Req.</a>
        </li>
    
        <li>
            <a href="notifications.html"><i class="fa fa-envelope"></i>Messages</a>
        </li>
    </ul>
</div>