@extends('layouts.app')

@section('content')

<!--============= Hero Section Starts Here =============-->
<div class="hero-section">
    <div class="container">
        <ul class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li>
                <a href="{{url('/seller-account')}}">Account</a>
            </li>
            <li>
                <span>Profile</span>
            </li>
        </ul>
    </div>
    <div class="bg_img hero-bg bottom_center" data-background="{{ asset('assets/images/banner/hero-bg.png') }}"></div>
</div>
<!--============= Hero Section Ends Here =============-->


<!--============= Profile Section Starts Here =============-->
<section class="dashboard-section padding-bottom mt--240 mt-lg--440 pos-rel">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-sm-10 col-md-7 col-lg-4">
                @include('components.profile_side_view_seller', ['data'=> []])
                    
                </div>
                <div class="col-lg-8">
                    <div class="row">
                        <div class="col-12">
                            <div class="dash-pro-item mb-30 dashboard-widget">
                                <div class="header">
                                    <h4 class="title">Personal Details</h4>
                                    <span class="edit"><a href="{{url('/edit-profile')}}"><i class="flaticon-edit"></i> Edit</a></span>
                                </div>
                                <ul class="dash-pro-body">
                                    <li>
                                        <div class="info-name">First Name</div>
                                        <div class="info-value">Andy</div>
                                    </li>
                                    <li>
                                        <div class="info-name">Last Name</div>
                                        <div class="info-value">Bernard</div>
                                    </li>
                                    <li>
                                        <div class="info-name">NIC</div>
                                        <div class="info-value">820010650V</div>
                                    </li>
                                    <li>
                                        <div class="info-name">Address</div>
                                        <div class="info-value">10A, Royal Residencies</div>
                                    </li>
                                    <li>
                                        <div class="info-name">City</div>
                                        <div class="info-value">Ja Ela</div>
                                    </li>
                                    <li>
                                        <div class="info-name">Phone</div>
                                        <div class="info-value">0701005080</div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="dash-pro-item mb-30 dashboard-widget">
                                <div class="header">
                                    <h4 class="title">Account Settings</h4>
                                </div>
                                <ul class="dash-pro-body">
                                    <li>
                                        <div class="info-name">Email</div>
                                        <div class="info-value">andyb@dundermifflin.com</div>
                                    </li>
                                    <li>
                                        <div class="info-name">Status</div>
                                        <div class="info-value"><i class="flaticon-check text-success"></i> Active</div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="dash-pro-item dashboard-widget">
                                <div class="header">
                                    <h4 class="title">Security</h4>
                                </div>
                                <ul class="dash-pro-body">
                                    <li>
                                        <div class="info-name">Password</div>
                                        <div class="info-value">********    <a href="">(Change Password)</a></div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<!--============= Profile Section Ends Here =============-->

@endsection
