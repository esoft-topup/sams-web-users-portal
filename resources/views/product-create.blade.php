@extends('layouts.app')

@section('content')

<!--============= Hero Section Starts Here =============-->
<div class="hero-section">
    <div class="container">
        <ul class="breadcrumb">
            <li>
                <a href="{{url('/')}}">Home</a>
            </li>
            <li>
                <a href="{{url('/seller-account')}}">Account</a>
            </li>
            <li>
                <a href="{{url('/seller-account')}}">My Products</a>
            </li>
            <li>
                <span>Create</span>
            </li>
        </ul>
    </div>
    <div class="bg_img hero-bg bottom_center" data-background="{{ asset('assets/images/banner/hero-bg.png') }}"></div>
</div>
<!--============= Hero Section Ends Here =============-->


<!--============= Profile Section Starts Here =============-->
<section class="dashboard-section padding-bottom mt--240 mt-lg--440 pos-rel">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-3">
                @include('components.profile_side_view_seller', ['data'=> []])
            </div>
            <div class="col-9">
                <div class="dash-pro-item dashboard-widget">
                    <div class="header">
                        <h4 class="title">Create a new Product</h4>
                    </div>
                    <form class="login-form" method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="mb-20">
                            <p>Category</p>
                        </div>
                        <div class="form-group select mb-30 @error('categories_id') is-invalid @enderror">

                            <select class="form-control" name="categories_id" id="categories" required>
                                <option value="" selected disabled>Select a Category</option>

                                @foreach($categories as $key => $category)
                                <option value="{{ $category->id }}">{{ $category->category_name }}</option>
                                @endforeach

                                @error('categories_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </select>
                        </div>
                        <div class="mb-20">
                            <p>Product Name</p>
                        </div>
                        <div class="custom-box mb-30 @error('product_name') is-invalid @enderror">

                            <input id="product_name" type="text" name="product_name" required>

                            @error('last_name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="mb-20">
                            <p>Description</p>
                        </div>
                        <div class="custom-box mb-30 @error('description') is-invalid @enderror">

                            <textarea id="description" name="description" rows="2" required></textarea>

                            @error('description')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="mb-20">
                            <p>Images</p>
                        </div>
                        <div class="custom-box-files mb-30 @error('files') is-invalid @enderror">

                            <input id="files" type="file" name="files" required multiple>

                            @error('files')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="mb-20">
                            <p>Starting Date Time</p>
                        </div>
                        <div class="custom-box mb-30 @error('start_date') is-invalid @enderror">

                            <input id="start_date" type="datetime-local" name="start_date" required>

                            @error('start_date')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="mb-20">
                            <p>Closing Date Time</p>
                        </div>
                        <div class="custom-box mb-30 @error('closing_date') is-invalid @enderror">

                            <input id="closing_date" type="datetime-local" name="closing_date" required>

                            @error('closing_date')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="mb-20">
                            <p>Starting Price</p>
                        </div>
                        <div class="custom-box mb-30 @error('minimum_price') is-invalid @enderror">

                            <input id="minimum_price" type="text" name="minimum_price" required>

                            @error('minimum_price')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group mb-0">
                            <button type="submit" class="custom-button">Create</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>

</section>
<!--============= Profile Section Ends Here =============-->

@endsection
